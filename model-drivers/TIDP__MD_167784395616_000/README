#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the Common Development
# and Distribution License Version 1.0 (the "License").
#
# You can obtain a copy of the license at
# http://www.opensource.org/licenses/CDDL-1.0.  See the License for the
# specific language governing permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each file and
# include the License file in a prominent location with the name LICENSE.CDDL.
# If applicable, add the following below this CDDL HEADER, with the fields
# enclosed by brackets "[]" replaced with your own identifying information:
#
# Portions Copyright (c) [yyyy] [name of copyright owner]. All rights reserved.
#
# CDDL HEADER END
#
#
# Copyright (c) 2013--2019, Regents of the University of Minnesota.
# All rights reserved.
#
# Contributors:
#    Ellad B. Tadmor
#

This directory contains a model driver written in C for the interatomic pair
potential with tunable intrinsic ductility by

V. P. Rajan, D. H. Warner and W. A. Curtin,
Model. Simul. Mater. Sci. Eng., 24:025005, 2016.
https://doi.org/10.1088/0965-0393/24/2/025005

This is a toy model designed to work with the `user01` species. It expects
length and energy units of A and eV, respectively, however these are proxy
units since the model definition is dimensionless.

To create a KIM Model from this Model Driver, one parameter file is required.
This file must have the following format:

   Line  1: `SpeciesName' string with element symbol (this should be "user01")
   Line  2:    `alpha' Region 1 exponent (1/A)
   Line  3:    `r1'    region 1 upper radius (A)
   Line  4:    `r2'    region 2 upper radius (A)
   Line  5:    `r3'    region 3 upper radius (and cutoff) (A)
   Line  6:    `A1'    region 2 polynomial cubic parameter (eV/A^3)
   Line  7:    `B1'    region 2 polynomial quadratic parameter (eV/A^2)
   Line  8:    `C1'    region 2 polynomial linear parameter (eV/A)
   Line  9:    `D1'    region 2 polynomial constant parameter (eV)
   Line 10:    `A2'    region 3 polynomial cubic parameter (eV/A^3)
   Line 11:    `B2'    region 3 polynomial quadratic parameter (eV/A^2)
   Line 12:    `C2'    region 3 polynomial linear parameter (eV/A)
   Line 13:    `D2'    region 3 polynomial constant parameter (eV)

Any additional lines will be silently ignored.

The parameters in the A to F parameterizations for this model driver were
generated using the Matlab script tunable_potentials_3D.m provided by
Andric Predrag from Prof. Bill Curtin's group at EPFL. These values differ
slightly (second digit) from the values in Table 4 in the paper cited above.
The new values ensure continuity of the potentials and are provided to
higher precision.

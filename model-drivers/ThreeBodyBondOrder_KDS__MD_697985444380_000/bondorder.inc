/*                                                                            */
/* CDDL HEADER START                                                          */
/*                                                                            */
/* The contents of this file are subject to the terms of the Common           */
/* Development and Distribution License Version 1.0 (the "License").          */
/*                                                                            */
/* You can obtain a copy of the license at                                    */
/* http://www.opensource.org/licenses/CDDL-1.0.  See the License for the      */
/* specific language governing permissions and limitations under the License. */
/*                                                                            */
/* When distributing Covered Code, include this CDDL HEADER in each file and  */
/* include the License file in a prominent location with the name             */
/* LICENSE.CDDL.  If applicable, add the following below this CDDL HEADER,    */
/* with the fields enclosed by brackets "[]" replaced with your own           */
/* identifying information:                                                   */
/*                                                                            */
/* Portions Copyright (c) [yyyy] [name of copyright owner].                   */
/* All rights reserved.                                                       */
/*                                                                            */
/* CDDL HEADER END                                                            */
/*                                                                            */

/*                                                                            */
/* Portions Copyright (c) 2019, Regents of the University of Minnesota.       */
/* All rights reserved.                                                       */
/*                                                                            */
/* Contributors:                                                              */
/*    Daniel S. Karls                                                         */
/*    Ellad B. Tadmor                                                         */
/*    Ryan S. Elliott                                                         */
/*                                                                            */
/* Portions Copyright (c) 2019, Regents of the University of Minnesota.       */
/* All rights reserved.                                                       */
/*                                                                            */
/* Contributors:                                                              */
/*    Anshul Chawla                                                           */
/*                                                                            */

#include <math.h>

/* Model Driver for the Khor-Das Sarma three-body bond-order potential        */
/*                                                                            */
/* Source:                                                                    */
/* K.E.Khor and S. Das Sarma                                                  */
/* "Proposed universal interatomic potential for elemental tetrahedrally      */
/*  bonded semiconductors", Physical Review B, Vol. 38, 3318-3322, 1988.      */

/* The following flag indicates whether or not to do an explicit check to     */
/* determine if the distance between neighbors j and k of atom i in a         */
/* three-body computations exceeds the (single) cutoff distance.  The check   */
/* is skipped if the flag is set to 1, and performed if the flag is set to 0. */
/* This feature is provided for efficiency reasons.                           */
/*                                                                            */
#define SKIP_CHECK_ON_RJK 1

/* The following enumeration provides a named index to each element in your   */
/* parameter array.  The last entry *must* be NUM_PARAMS (by construction by  */
/* being at the end of the list, its value will be the number of parameters). */
/* These names will be used in the functions below to unpack and access       */
/* parameters.  The order of parameters *is* important.  It must match the    */
/* ordering in the parameter file read in by the model driver.                */
/*                                                                            */
enum PARAMS {
  PARAM_A,
  PARAM_B,
  PARAM_theta,
  PARAM_lambda,
  PARAM_alpha,
  PARAM_beta,
  PARAM_gamma,
  PARAM_eta,
  PARAM_R,
  PARAM_thetanot,
  NUM_PARAMS
};

#include "bondorder_aux.inc"

/* The following array of strings contains names and descriptions of the      */
/* parameters that will be registered in the KIM API object. The number of    */
/* entries must match the number of PARAM_* lines above, and the order must   */
/* correspond to the ordering above.                                          */
/*                                                                            */
static char const * const param_strings[][2]
  = {{"A", "Amplitude of the cutoff function"},
    {"B", "Parameter in the prefactor b_ij to the attractive pairwise interaction"},
    {"theta", "Exponent in the repulsive pairwise interaction function f_R"},
    {"lambda", "Exponent in the attractive pairwise interaction function f_A"},
    {"alpha", "Parameter in the prefactor b_ij to the attractive pairwise interaction"},
    {"beta", "Parameter in the prefactor a_ij and b_ij to the repulsive pairwise interaction and attractive pairwise interaction respectively"},
    {"gamma", "Parameter in the prefactor a_ij and b_ij to the repulsive pairwise interaction and attractive pairwise interaction respectively"},
    {"eta", "Parameter in the angular function g(theta)"},
    {"R", "minimum interatomic distance"},
    {"thetanot","Equilibrium bond angle"}};

/* The following two variables define the base unit system required to be     */
/* used by the parameter file.                                                */
/*                                                                            */
static KIM_LengthUnit const * const length_unit = &KIM_LENGTH_UNIT_A;
static KIM_EnergyUnit const * const energy_unit = &KIM_ENERGY_UNIT_eV;

/* The following array of double precision values define the unit             */
/* exponents for each parameter.  The first number is the length exponent     */
/* and the second number is the energy exponent. The number of entries must   */
/* match the number of PARAM_* lines above, and the order must correspond     */
/* to the ordering above. Use two zeros for unitless parameters.              */
/*                                                                            */
static double const param_units[][2] = {{0.0, 1.0},  /* A length energy */
                                        {0.0, 0.0},  /* B */
                                        {-1.0, 0.0}, /* theta */
                                        {-1.0, 0.0}, /* lambda */
                                        {0.0, 0.0},  /* alpha */
                                        {-1.0, 0.0}, /* beta (gamma) */
                                        {0.0, 0.0},  /* gamma */
                                        {0.0, 0.0},  /* eta */
                                        {1.0, 0.0},  /* R */
                                        {0.0, 0.0}}; /* theta */

static double get_influence_distance(double const * const params)
{
  double const beta = params[PARAM_beta];
  double const gamma = params[PARAM_gamma];
  double const R = params[PARAM_R];

  return (R + pow(-log(1e-16)/beta, 1/gamma));
}

/* Calculate coordination Zi and its derivative w.r.t. Rij */
static void calc_Zi_dZi(double const * const params,
                        double const Rij,
                        double * const Z_i,
                        double * const dZi_dRij)
{
  /* Unpack parameters */

  double const beta = params[PARAM_beta];
  double const gamma = params[PARAM_gamma];
  double const R = params[PARAM_R];

  if (Rij <= R)
  {
    *Z_i = 1.0;

    if (dZi_dRij != NULL)
    {
      *dZi_dRij = 0.0;
    }
  }
  else
  {
    *Z_i = exp(-beta * pow(Rij - R, gamma));

    if (dZi_dRij != NULL)
    {
      *dZi_dRij = -beta * gamma * pow(Rij - R, gamma - 1.0) * (*Z_i);
    }
  }

  return;
}

/* Calculate bond order bij and its derivative w.r.t. zeta_ij */
static void calc_bij_dbij(double const * const params,
                          double const Z_i,
                          double const zeta_ij,
                          double * const bij,
                          double * const dbij_dZi,
                          double * const dbij_dzeta_ij)
{
  /* Unpack parameters */
  double const B = params[PARAM_B];
  double const alpha = params[PARAM_alpha];

  double const Zi_pow_alpha = pow(Z_i, alpha);

  *bij = (B/Zi_pow_alpha) *(1+ zeta_ij);

  if (dbij_dZi != NULL)
  {
    *dbij_dZi = -(alpha/(Z_i)) * *bij;
    *dbij_dzeta_ij = (B/Zi_pow_alpha);
  }

  return;
}

/* Calculate two-body energy phi2(r) and its derivative w.r.t. Rij */
static void calc_phi2_dphi2(double const * const params,
                            double const Rij,
                            double const bij,
                            double * const phi2,
                            double * const dphi2_dRij,
                            double * const dphi2_dbij)
{
  *phi2 = fc(params, Rij) * (fR(params, Rij) + bij * fA(params, Rij));

  if (dphi2_dRij != NULL)
  {
    *dphi2_dRij
        = fc(params, Rij)
              * (dfR_dRij(params, Rij) + bij * dfA_dRij(params, Rij))
          + dfc_dR(params, Rij) * (fR(params, Rij) + bij * fA(params, Rij));

    *dphi2_dbij = fc(params, Rij) * fA(params, Rij);
  }

  return;
}

/* Calculate three-body energy phi3(Rij, Rik, Rjk) and its derivatives w.r.t
 * Rij, Rik, and Rjk */
static void calc_phi3_dphi3(double const * const params,
                            double const Rij,
                            double const Rik,
                            double const Rjk,
                            double * const phi3,
                            double * const dphi3_dRij,
                            double * const dphi3_dRik,
                            double * const dphi3_dRjk)
{
  /* Unpack parameters */

  double theta_jik;
  double gval;
  double dcosthetajik_dRij;
  double dcosthetajik_dRik;
  double dcosthetajik_dRjk;
  double dg_dcosthetaval;

  theta_jik = acos((Rij * Rij + Rik * Rik - Rjk * Rjk) / (2 * Rij * Rik));
  gval = g(params, theta_jik);

  *phi3 = gval;

  if (dphi3_dRij != NULL)
  {
    dg_dcosthetaval = dg_dcostheta(params, theta_jik);

    dcosthetajik_dRij
        = (Rij * Rij - Rik * Rik + Rjk * Rjk) / (2 * Rij * Rij * Rik);
    dcosthetajik_dRik
        = (-Rij * Rij + Rik * Rik + Rjk * Rjk) / (2 * Rij * Rik * Rik);
    dcosthetajik_dRjk = -Rjk / (Rij * Rik);

    *dphi3_dRij = (dg_dcosthetaval * dcosthetajik_dRij);

    *dphi3_dRik = (dg_dcosthetaval * dcosthetajik_dRik);

    *dphi3_dRjk = (dg_dcosthetaval * dcosthetajik_dRjk);


   }

  return;
}

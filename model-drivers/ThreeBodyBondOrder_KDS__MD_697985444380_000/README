#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the Common Development
# and Distribution License Version 1.0 (the "License").
#
# You can obtain a copy of the license at
# http://www.opensource.org/licenses/CDDL-1.0.  See the License for the
# specific language governing permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each file and
# include the License file in a prominent location with the name LICENSE.CDDL.
# If applicable, add the following below this CDDL HEADER, with the fields
# enclosed by brackets "[]" replaced with your own identifying information:
#
# Portions Copyright (c) [yyyy] [name of copyright owner]. All rights reserved.
#
# CDDL HEADER END
#

#
# Portions Copyright (c) 2019, Regents of the University of Minnesota.
# All rights reserved.
#
# Contributors:
#    Daniel S. Karls
#    Ellad B. Tadmor
#    Ryan S. Elliott
#
# Portions Copyright (c) 2019, Regents of the University of Minnesota.
# All rights reserved.
#
# Contributors:
#     Anshul Chawla 
#

Model driver for the Khor-Das Sarma three-body bond-order potential
presented in

K.E.Khor and S. Das Sarma (1988)                                                 
"Proposed universal interatomic potential for elemental tetrahedrally      
bonded semiconductors", Physical Review B, Vol. 38, 3318-3322, 1988.     

This driver reads in one parameter file with the species followed by
ten parameters (required units indicated in square brackets following
the parameter):

species
A 	[eV]
B 	[unitless]
theta	[1/Angstrom]
lambda	[1/Angstrom]
alpha	[unitless]
beta	[unitless] 
gamma	[unitless]
eta	[unitless]
R	[Angstrom]
thetanot [radians]

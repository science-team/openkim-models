/*                                                                            */
/* CDDL HEADER START                                                          */
/*                                                                            */
/* The contents of this file are subject to the terms of the Common           */
/* Development and Distribution License Version 1.0 (the "License").          */
/*                                                                            */
/* You can obtain a copy of the license at                                    */
/* http://www.opensource.org/licenses/CDDL-1.0.  See the License for the      */
/* specific language governing permissions and limitations under the License. */
/*                                                                            */
/* When distributing Covered Code, include this CDDL HEADER in each file and  */
/* include the License file in a prominent location with the name             */
/* LICENSE.CDDL.  If applicable, add the following below this CDDL HEADER,    */
/* with the fields enclosed by brackets "[]" replaced with your own           */
/* identifying information:                                                   */
/*                                                                            */
/* Portions Copyright (c) [yyyy] [name of copyright owner].                   */
/* All rights reserved.                                                       */
/*                                                                            */
/* CDDL HEADER END                                                            */
/*                                                                            */

/*                                                                            */
/* Portions Copyright (c) 2019, Regents of the University of Minnesota.       */
/* All rights reserved.                                                       */
/*                                                                            */
/* Contributors:                                                              */
/*    Daniel S. Karls                                                         */
/*    Ellad B. Tadmor                                                         */
/*    Ryan S. Elliott                                                         */
/*                                                                            */
/* Portions Copyright (c) 2019, Regents of the University of Minnesota.       */
/* All rights reserved.                                                       */
/*                                                                            */
/* Contributors:                                                              */
/*    Dipanjan Ghosh                                                          */
/*                                                                            */

#include <math.h>

/* Model driver for the Biswas-Hamann (BH) potential                          */
/*                                                                            */
/* Source:                                                                    */
/* Biswas R, Hamann DR (1987),                                                */
/* New classical models for silicon structural energies.                      */
/* Physical Review B 36(12), 6434. doi:10.1103/PhysRevB.36.6434               */
/*                                                                            */

/* The following flag indicates whether or not to do an explicit check to     */
/* determine if the distance between neighbors j and k of atom i in a         */
/* three-body computations exceeds the (single) cutoff distance.  The check   */
/* is skipped if the flag is set to 1, and performed if the flag is set to 0. */
/* This feature is provided for efficiency reasons.                           */
/*                                                                            */
#define SKIP_CHECK_ON_RJK 1

/* The following enumeration provides a named index to each element in your   */
/* parameter array.  The last entry *must* be NUM_PARAMS (by construction by  */
/* being at the end of the list, its value will be the number of parameters). */
/* These names will be used in the functions below to unpack and access       */
/* parameters.  The order of parameters *is* important.  It must match the    */
/* ordering in the parameter file read in by the model driver.                */
/*                                                                            */
enum PARAMS {
  PARAM_A1,
  PARAM_A2,
  PARAM_B1,
  PARAM_B2,
  PARAM_lambda1,
  PARAM_lambda2,
  PARAM_alpha1,
  PARAM_alpha2,
  PARAM_rc,
  PARAM_mu,
  PARAM_rcutoff,
  NUM_PARAMS
};

#include "cluster_aux.inc"


/* The following array of strings contains names and descriptions of the      */
/* parameters that will be registered in the KIM API object. The number of    */
/* entries must match the number of PARAM_* lines above, and the order must   */
/* correspond to the ordering above.                                          */
/*                                                                            */
static char const * const param_strings[][2]
    = {{"A1", "Amplitude of the pairwise interaction function"},
       {"A2", "Another amplitude of the pairwise interaction function"},
       {"B1", "Amplitude of the three-body term"},
       {"B2", "Another amplitude of the three-body term"},
       {"lambda1", "Another exponent in the pairwise interaction"},
       {"lambda2", "Exponent in the pairwise interaction"},
       {"alpha1", "Exponent in the three-body term"},
       {"alpha2", "Another exponent in the three-body term"},
       {"rc", "Parameter in cutoff function"},
       {"mu", "Scale in cutoff function"},
       {"rcutoff", "Influence distance"}};

/* The following two variables define the base unit system required to be     */
/* used by the parameter file.                                                */
/*                                                                            */
static KIM_LengthUnit const * const length_unit = &KIM_LENGTH_UNIT_A;
static KIM_EnergyUnit const * const energy_unit = &KIM_ENERGY_UNIT_eV;

/* The following array of double precision values define the unit             */
/* exponents for each parameter.  The first number is the length exponent     */
/* and the second number is the energy exponent. The number of entries must   */
/* match the number of PARAM_* lines above, and the order must correspond     */
/* to the ordering above. Use two zeros for unitless parameters.              */
/*                                                                            */
static double const param_units[][2] = {{0.0, 1.0},  /* A1 */
                                        {0.0, 1.0},  /* A2 */
                                        {0.0, 1.0},  /* B1 */
                                        {0.0, 1.0},  /* B2 */
                                        {-2.0, 0.0}, /* lambda1 */
                                        {-2.0, 0.0}, /* lambda2 */
                                        {-2.0, 0.0}, /* alpha1 */
                                        {-2.0, 0.0}, /* alpha2 */
                                        {1.0, 0.0},  /* rc */
                                        {1.0, 0.0},  /* mu */
                                        {1.0, 0.0}}; /* rcutoff */

static double get_influence_distance(double const * const params)
{
  /*double const a = params[PARAM_a];*/
  /*double const sigma = params[PARAM_sigma];*/
  double const rcutoff = params[PARAM_rcutoff];

  return rcutoff;
}

/* Calculate two-body energy phi2(r) and its derivative w.r.t. Rij */
static void calc_phi2_dphi2(double const * const params,
                            double const Rij,
                            double * const phi2,
                            double * const dphi2_dRij)
{
  /* Unpack parameters */
  /*double const epsilon = params[PARAM_epsilon]; */
  /*double const sigma = params[PARAM_sigma]; */

  double f2val;
  double df2_dRij;

  if (dphi2_dRij != NULL) 
  {
     f2_df2(params, Rij, &f2val, &df2_dRij);
     *phi2 = f2val;
     *dphi2_dRij = df2_dRij;
  }
  else
  {
     f2_df2(params, Rij, &f2val, NULL);
     *phi2 = f2val;
  }

  return;
}

/* Calculate three-body energy phi3(Rij, Rik, Rjk) and its derivatives w.r.t
   Rij, Rik, and Rjk */
static void calc_phi3_dphi3(double const * const params,
                            double const Rij,
                            double const Rik,
                            double const Rjk,
                            double * const phi3,
                            double * const dphi3_dRij,
                            double * const dphi3_dRik,
                            double * const dphi3_dRjk)
{
  /* Unpack parameters */

  double f3val;
  double df3_dRij;
  double df3_dRik;
  double df3_dRjk;

  if (dphi3_dRij != NULL)
  {
    f3_df3(params,
           Rij,
           Rik,
           Rjk,
           &f3val,
           &df3_dRij,
           &df3_dRik,
           &df3_dRjk);
    *phi3 = f3val;
    *dphi3_dRij = df3_dRij;
    *dphi3_dRik = df3_dRik;
    *dphi3_dRjk = df3_dRjk;
  }
  else
  {
    f3_df3(params,
           Rij,
           Rik,
           Rjk,
           &f3val,
           NULL,
           NULL,
           NULL);
    *phi3 = f3val;
  }
  return;
}


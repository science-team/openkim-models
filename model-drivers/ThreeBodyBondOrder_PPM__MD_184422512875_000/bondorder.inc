/*                                                                            */
/* CDDL HEADER START                                                          */
/*                                                                            */
/* The contents of this file are subject to the terms of the Common           */
/* Development and Distribution License Version 1.0 (the "License").          */
/*                                                                            */
/* You can obtain a copy of the license at                                    */
/* http://www.opensource.org/licenses/CDDL-1.0.  See the License for the      */
/* specific language governing permissions and limitations under the License. */
/*                                                                            */
/* When distributing Covered Code, include this CDDL HEADER in each file and  */
/* include the License file in a prominent location with the name             */
/* LICENSE.CDDL.  If applicable, add the following below this CDDL HEADER,    */
/* with the fields enclosed by brackets "[]" replaced with your own           */
/* identifying information:                                                   */
/*                                                                            */
/* Portions Copyright (c) [yyyy] [name of copyright owner].                   */
/* All rights reserved.                                                       */
/*                                                                            */
/* CDDL HEADER END                                                            */
/*                                                                            */

/*                                                                            */
/* Portions Copyright (c) 2019, Regents of the University of Minnesota.       */
/* All rights reserved.                                                       */
/*                                                                            */
/* Contributors:                                                              */
/*    Daniel S. Karls                                                         */
/*    Ellad B. Tadmor                                                         */
/*    Ryan S. Elliott                                                         */
/*    Moon-Ki Choi
/*                                                                            */

#include <math.h>

/*                                                                            */
/* Model driver for a bond-order potential based on a generalized Tersoff     */
/* form, originally designed to provide a better representation of bulk and   */
/* 2D silicon.  The potential source is:                                      */
/*                                                                            */
/* G. P. Purja Pun and Y. Mishin, Phys. Rev. B, 95(21):224103, 2017,          */
/* doi: 10.1103/PhysRevB.95.224103                                            */
/*                                                                            */

/* The following flag indicates whether or not to do an explicit check to     */
/* determine if the distance between neighbors j and k of atom i in a         */
/* three-body computations exceeds the (single) cutoff distance.  The check   */
/* is skipped if the flag is set to 1, and performed if the flag is set to 0. */
/* This feature is provided for efficiency reasons.                           */
/*                                                                            */
#define SKIP_CHECK_ON_RJK 1

/* The following enumeration provides a named index to each element in your   */
/* parameter array.  The last entry *must* be NUM_PARAMS (by construction by  */
/* being at the end of the list, its value will be the number of parameters). */
/* These names will be used in the functions below to unpack and access       */
/* parameters.  The order of parameters *is* important.  It must match the    */
/* ordering in the parameter file read in by the model driver.                */
/*                                                                            */
enum PARAMS {
  PARAM_A,
  PARAM_B,
  PARAM_lambda1,
  PARAM_lambda2,
  PARAM_eta,
  PARAM_delta,
  PARAM_alpha,
  PARAM_beta,
  PARAM_c0,
  PARAM_c1,
  PARAM_c2,
  PARAM_c3,
  PARAM_c4,
  PARAM_c5,
  PARAM_h,
  PARAM_R1,
  PARAM_R2,
  NUM_PARAMS
};

#include "bondorder_aux.inc"

/* The following array of strings contains names and descriptions of the      */
/* parameters that will be registered in the KIM API object. The number of    */
/* entries must match the number of PARAM_* lines above, and the order must   */
/* correspond to the ordering above.                                          */
/*                                                                            */
static char const * const param_strings[][2]
    = {{"A", "Amplitude in the repulsive pairwise interaction function f_R"},
       {"B", "Amplitude in the attractive pairwise interaction function f_A"},
       {"lambda1", "Exponent in the repulsive pairwise interaction function f_R"},
       {"lambda2", "Exponent in the attractive pairwise interaction function f_A"},
       {"eta","Exponent in the zeta_ij term in the bond-order function b_ij"},
       {"delta","Exponent in the bond-order b_ij"},
       {"alpha", "Parameter in the prefactor a_ij to the repulsive pairwise interaction"},
       {"beta", "Parameter in the prefactor b_ij to the attractive pairwise interaction"},
       {"c0","Parameter in the angular function g(costheta)"},
       {"c1","Parameter in the angular function g(costheta)"},
       {"c2","Parameter in the angular function g(costheta)"},
       {"c3","Parameter in the angular function g(costheta)"},
       {"c4","Parameter in the angular function g(costheta)"},
       {"c5","Parameter in the angular function g(costheta)"},
       {"h", "Parameter in the angular function g(theta)"},
       {"R1", "Internal cutoff radius below which cutoff function f_C equals one"},
       {"R2", "Width of which cutoff function f_C goes from one to zero"}};

/* The following two variables define the base unit system required to be     */
/* used by the parameter file.                                                */
/*                                                                            */
static KIM_LengthUnit const * const length_unit = &KIM_LENGTH_UNIT_A;
static KIM_EnergyUnit const * const energy_unit = &KIM_ENERGY_UNIT_eV;

/* The following array of double precision values define the unit             */
/* exponents for each parameter.  The first number is the length exponent     */
/* and the second number is the energy exponent. The number of entries must   */
/* match the number of PARAM_* lines above, and the order must correspond     */
/* to the ordering above. Use two zeros for unitless parameters.              */
/*                                                                            */
static double const param_units[][2] = {{0.0, 1.0},  /* A */
                                        {0.0, 1.0},  /* B */
                                        {-1.0, 0.0}, /* lambda1 */
                                        {-1.0, 0.0}, /* lambda2 */
                                        {0.0, 0.0},  /* eta */
                                        {0.0, 0.0},  /* delta */
                                        {0.0, 0.0},  /* alpha */
                                        {0.0, 0.0},  /* beta */
                                        {0.0, 1.0},  /* c0 */
                                        {0.0, 0.0},  /* c1 */
                                        {0.0, 0.0},  /* c2 */
					{0.0, 0.0},  /* c3 */
					{0.0, 0.0},  /* c4 */
					{0.0, 0.0},  /* c5 */
					{0.0, 0.0},  /* h  */
                                        {1.0, 0.0},  /* R1 */
                                        {1.0, 0.0}}; /* R2 */

static double get_influence_distance(double const * const params)
{
  double const R2 = params[PARAM_R2];

  return R2;
}

/* Calculate bond order bij and its derivative w.r.t. zeta_ij */
static void calc_bij_dbij(double const * const params,
                          double const zeta_ij,
                          double * const bij,
                          double * const dbij_dzeta_ij)
{
  /* Unpack parameters */


  double const delta = params[PARAM_delta];
  double const eta = params[PARAM_eta];

  double const zeta_pow_eta = pow(zeta_ij, eta);

  *bij = pow(1.0 + zeta_pow_eta, -delta);

  if (dbij_dzeta_ij != NULL) 
  { 
    *dbij_dzeta_ij = -(delta) * pow(1 + zeta_pow_eta, -delta - 1.0) * eta
		                 * zeta_pow_eta / zeta_ij;
  }




  return;
}

/* Calculate two-body energy phi2(r) and its derivative w.r.t. Rij */
static void calc_phi2_dphi2(double const * const params,
                            double const Rij,
                            double const bij,
                            double * const phi2,
                            double * const dphi2_dRij,
                            double * const dphi2_dbij)
{
  double const c0 = params[PARAM_c0]; 
  *phi2 = fc(params, Rij) * (fR(params, Rij) + bij * fA(params, Rij) + c0);

  if (dphi2_dRij != NULL)
  {
    *dphi2_dRij
        = fc(params, Rij)
              * (dfR_dRij(params, Rij) + bij * dfA_dRij(params, Rij))
          + dfc_dR(params, Rij) * (fR(params, Rij) + bij * fA(params, Rij) +c0);

    *dphi2_dbij = fc(params, Rij) * fA(params, Rij);
  }

  return;
}

/* Calculate three-body energy phi3(Rij, Rik, Rjk) and its derivatives w.r.t
 * Rij, Rik, and Rjk */
static void calc_phi3_dphi3(double const * const params,
                            double const Rij,
                            double const Rik,
                            double const Rjk,
                            double * const phi3,
                            double * const dphi3_dRij,
                            double * const dphi3_dRik,
                            double * const dphi3_dRjk)
{
  /* Unpack parameters */
  double const alpha = params[PARAM_alpha];
  double const beta  = params[PARAM_beta];

  double costheta_jik;
  double gval;
  double arg_exp;
  double exponential;
  double dcosthetajik_dRij;
  double dcosthetajik_dRik;
  double dcosthetajik_dRjk;
  double dexponential_dRij;
  double dexponential_dRik;
  double dg_dcosthetaval;

  costheta_jik = (Rij * Rij + Rik * Rik - Rjk * Rjk) / (2 * Rij * Rik);
  gval = g(params, costheta_jik);

  arg_exp = (Rij - Rik);
  arg_exp = alpha * (pow(arg_exp,beta));
  exponential = exp(arg_exp);

  *phi3 = fc(params, Rik) * gval * exponential;

  if (dphi3_dRij != NULL)
  {
    dg_dcosthetaval = dg_dcostheta(params, costheta_jik);

    dcosthetajik_dRij
        = (Rij * Rij - Rik * Rik + Rjk * Rjk) / (2 * Rij * Rij * Rik);
    dcosthetajik_dRik
        = (-Rij * Rij + Rik * Rik + Rjk * Rjk) / (2 * Rij * Rik * Rik);
    dcosthetajik_dRjk = -Rjk / (Rij * Rik);

    dexponential_dRij = alpha * beta * pow((Rij - Rik), (beta - 1.0)) 
                               * exponential;

    dexponential_dRik = -dexponential_dRij;

    *dphi3_dRij = fc(params, Rik)
                  * (gval * dexponential_dRij
                     + exponential * dg_dcosthetaval * dcosthetajik_dRij);

    *dphi3_dRik = fc(params, Rik)
                      * (gval * dexponential_dRik
                         + exponential * dg_dcosthetaval * dcosthetajik_dRik)
                  + dfc_dR(params, Rik) * gval * exponential;

    *dphi3_dRjk
        = fc(params, Rik) * dg_dcosthetaval * dcosthetajik_dRjk * exponential;
  }

  return;
}

/*                                                                            */
/* CDDL HEADER START                                                          */
/*                                                                            */
/* The contents of this file are subject to the terms of the Common           */
/* Development and Distribution License Version 1.0 (the "License").          */
/*                                                                            */
/* You can obtain a copy of the license at                                    */
/* http://www.opensource.org/licenses/CDDL-1.0.  See the License for the      */
/* specific language governing permissions and limitations under the License. */
/*                                                                            */
/* When distributing Covered Code, include this CDDL HEADER in each file and  */
/* include the License file in a prominent location with the name             */
/* LICENSE.CDDL.  If applicable, add the following below this CDDL HEADER,    */
/* with the fields enclosed by brackets "[]" replaced with your own           */
/* identifying information:                                                   */
/*                                                                            */
/* Portions Copyright (c) [yyyy] [name of copyright owner].                   */
/* All rights reserved.                                                       */
/*                                                                            */
/* CDDL HEADER END                                                            */
/*                                                                            */

/*                                                                            */
/* Portions Copyright (c) 2019, Regents of the University of Minnesota.       */
/* All rights reserved.                                                       */
/*                                                                            */
/* Contributors:                                                              */
/*    Daniel S. Karls                                                         */
/*    Ellad B. Tadmor                                                         */
/*    Ryan S. Elliott                                                         */
/*    Ben C. Druecke                                                          */
/*                                                                            */

#include <math.h>

/*                                                                            */
/* Model driver for the Stephenson-Radny-Smith (SRS) potential                */
/*                                                                            */
/* Source:                                                                    */
/* P. C. L. Stephenson, M. W. Radny and P. V. Smith,                          */
/* "A modified Stillinger-Weber potential for modeling silicon surfaces",     */
/* Surface Science, Vol. 366, 177-184, 1996.                                  */
/*                                                                            */

/* The following flag indicates whether or not to do an explicit check to     */
/* determine if the distance between neighbors j and k of atom i in a         */
/* three-body computations exceeds the (single) cutoff distance.  The check   */
/* is skipped if the flag is set to 1, and performed if the flag is set to 0. */
/* This feature is provided for efficiency reasons.                           */
/*                                                                            */
#define SKIP_CHECK_ON_RJK 1

/* The following enumeration provides a named index to each element in your   */
/* parameter array.  The last entry *must* be NUM_PARAMS (by construction by  */
/* being at the end of the list, its value will be the number of parameters). */
/* These names will be used in the functions below to unpack and access       */
/* parameters.  The order of parameters *is* important.  It must match the    */
/* ordering in the parameter file read in by the model driver.                */
/*                                                                            */
enum PARAMS {
  PARAM_A,
  PARAM_B,
  PARAM_p,
  PARAM_q,
  PARAM_a,
  PARAM_lambda,
  PARAM_gamma,
  PARAM_zeta,
  PARAM_b,
  PARAM_k,
  PARAM_c,
  PARAM_sigma,
  PARAM_epsilon,
  NUM_PARAMS
};

#include "cluster_aux.inc"


/* The following array of strings contains names and descriptions of the      */
/* parameters that will be registered in the KIM API object. The number of    */
/* entries must match the number of PARAM_* lines above, and the order must   */
/* correspond to the ordering above.                                          */
/*                                                                            */
static char const * const param_strings[][2]
    = {{"A", "Amplitude of the pairwise interaction function"},
       {"B", "Prefactor of r^-p in pairwise term"},
       {"p", "Exponent in the r^-p term in the pairwise interaction"},
       {"q", "Exponent in the r^-q term in the pairwise interaction"},
       {"a", "Non-dimensionalized cutoff distance appearing in the exponential cutoff function"},
       {"lambda", "Amplitude of the Stillinger-Weber h-function appearing int the three-body term"},
       {"gamma", "Scaling factor in numerator of terms inside the exponential cutoff function"},
       {"zeta", "Numerator of argument of exponential in pairwise interaction"},
       {"b", "Amplitude factor in angular dependence in three-body term"},
       {"k", "Factor governing equilibrium angle in three-body term: cos(theta)=-k"},
       {"c", "Offset in amplitude of angular dependence term"},
       {"sigma", "Length scale"},
       {"epsilon", "Energy scale"}};

/* The following two variables define the base unit system required to be     */
/* used by the parameter file.                                                */
/*                                                                            */
static KIM_LengthUnit const * const length_unit = &KIM_LENGTH_UNIT_A;
static KIM_EnergyUnit const * const energy_unit = &KIM_ENERGY_UNIT_eV;

/* The following array of double precision values define the unit             */
/* exponents for each parameter.  The first number is the length exponent     */
/* and the second number is the energy exponent. The number of entries must   */
/* match the number of PARAM_* lines above, and the order must correspond     */
/* to the ordering above. Use two zeros for unitless parameters.              */
/*                                                                            */
static double const param_units[][2] = {{0.0, 0.0},  /* A */
                                        {0.0, 0.0},  /* B */
                                        {0.0, 0.0},  /* p */
                                        {0.0, 0.0},  /* q */
                                        {0.0, 0.0},  /* a */
                                        {0.0, 0.0},  /* lambda */
                                        {0.0, 0.0},  /* gamma */
					{0.0, 0.0},  /* zeta */
					{0.0, 0.0},  /* b */
					{0.0, 0.0},  /* k */
					{0.0, 0.0},  /* c */
                                        {1.0, 0.0},  /* sigma */
                                        {0.0, 1.0}}; /* epsilon */

/* Function: get_influence_distance                                           */
/* Description: Sets the largest distance at which a neighbor can affect the  */
/*     energy of another atom.                                                */
static double get_influence_distance(double const * const params)
{

  double const a = params[PARAM_a];
  double const sigma = params[PARAM_sigma];

  return sigma * a;
}

/* Calculate two-body energy phi2(r) and its derivative w.r.t. Rij */
static void calc_phi2_dphi2(double const * const params,
                            double const Rij,
                            double * const phi2,
                            double * const dphi2_dRij)
{
  /* Unpack parameters */
  double const epsilon = params[PARAM_epsilon];
  double const sigma = params[PARAM_sigma];

  double f2val;
  double df2_dRij;

  f2_df2(params, Rij / sigma, &f2val, &df2_dRij);
  *phi2 = epsilon * f2val;

  if (dphi2_dRij != NULL) { *dphi2_dRij = epsilon * df2_dRij / sigma; }

  return;
}


/* Calculate three-body energy phi3(Rij, Rik, Rjk) and its derivatives w.r.t
   Rij, Rik, and Rjk */
static void calc_phi3_dphi3(double const * const params,
                            double const Rij,
                            double const Rik,
                            double const Rjk,
                            double * const phi3,
                            double * const dphi3_dRij,
                            double * const dphi3_dRik,
                            double * const dphi3_dRjk)
{
  /* Unpack parameters */
  double const epsilon = params[PARAM_epsilon];
  double const sigma = params[PARAM_sigma];

  double f3val;
  double df3_dRij;
  double df3_dRik;
  double df3_dRjk;

  f3_df3(params,
         Rij / sigma,
         Rik / sigma,
         Rjk / sigma,
         &f3val,
         &df3_dRij,
         &df3_dRik,
         &df3_dRjk);
  *phi3 = epsilon * f3val;

  if (dphi3_dRij != NULL)
  {
    *dphi3_dRij = epsilon * df3_dRij / sigma;
    *dphi3_dRik = epsilon * df3_dRik / sigma;
    *dphi3_dRjk = epsilon * df3_dRjk / sigma;
  }
  return;
}

#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the Common Development
# and Distribution License Version 1.0 (the "License").
#
# You can obtain a copy of the license at
# http://www.opensource.org/licenses/CDDL-1.0.  See the License for the
# specific language governing permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each file and
# include the License file in a prominent location with the name LICENSE.CDDL.
# If applicable, add the following below this CDDL HEADER, with the fields
# enclosed by brackets "[]" replaced with your own identifying information:
#
# Portions Copyright (c) [yyyy] [name of copyright owner]. All rights reserved.
#
# CDDL HEADER END
#

#
# Copyright (c) 2013--2019, Regents of the University of Minnesota.
# All rights reserved.
#
# Contributors:
#    Ryan S. Elliott
#    Ellad B. Tadmor
#    Valeriu Smirichinski
#    Hao Xu


This directory contains a Morse pair potential Model driver written in C.

   phi(r) = epsilon * ( - exp[ -2*C*(r - Rzero) ] + 2*exp[ -C*(r - Rzero) ] )
                    * S(r, logisticWidth, logisticR0)

where S(r, logisticWidth, logisticR0) is the logistic function.

   S(r, logisticWidth, logisticR0)
     = 1/(1 + exp( -logisticWidth*[r - logisticR0]))

The choice of

   logisticR0 = cutoff + 7/logisticWidth

is designed so that S(cutoff) ~ 0.001.

This model driver publishes its parameters, and supports optional
computation of `energy', `forces', `particleEnergy', `process_dEdr', and
`process_dE2dr2'.

To create a KIM Model from this Model Driver, a parameter file is required.
This file must have the following format:
   Line 1: Species name
   Line 2: `cutoff' value in Angstroms
   Line 3: `logisticWidth' value as pure number > 0.0
   Line 4: Morse `epsilon' value in eV
   Line 5: Morse `C' value in 1/Angstroms
   Line 6: Morse `Rzero' value in Angstroms
Any additional lines will be silently ignored.

  if (is_meam_c_) {
    switch (GetComputeIndex(is_compute_energy,
                            is_compute_forces, is_compute_particle_energy,
                            is_compute_virial, is_compute_particle_virial)) {
      case 0:
        ier = MeamCCompute<false, false, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 1:
        ier = MeamCCompute<false, false, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 2:
        ier = MeamCCompute<false, false, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 3:
        ier = MeamCCompute<false, false, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 4:
        ier = MeamCCompute<false, false, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 5:
        ier = MeamCCompute<false, false, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 6:
        ier = MeamCCompute<false, false, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 7:
        ier = MeamCCompute<false, false, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 8:
        ier = MeamCCompute<false, true, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 9:
        ier = MeamCCompute<false, true, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 10:
        ier = MeamCCompute<false, true, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 11:
        ier = MeamCCompute<false, true, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 12:
        ier = MeamCCompute<false, true, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 13:
        ier = MeamCCompute<false, true, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 14:
        ier = MeamCCompute<false, true, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 15:
        ier = MeamCCompute<false, true, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 16:
        ier = MeamCCompute<true, false, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 17:
        ier = MeamCCompute<true, false, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 18:
        ier = MeamCCompute<true, false, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 19:
        ier = MeamCCompute<true, false, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 20:
        ier = MeamCCompute<true, false, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 21:
        ier = MeamCCompute<true, false, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 22:
        ier = MeamCCompute<true, false, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 23:
        ier = MeamCCompute<true, false, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 24:
        ier = MeamCCompute<true, true, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 25:
        ier = MeamCCompute<true, true, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 26:
        ier = MeamCCompute<true, true, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 27:
        ier = MeamCCompute<true, true, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 28:
        ier = MeamCCompute<true, true, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 29:
        ier = MeamCCompute<true, true, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 30:
        ier = MeamCCompute<true, true, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 31:
        ier = MeamCCompute<true, true, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      default:
        HELPER_LOG_ERROR("Unknown compute function index");
        ier = true;
        break;
    }
  } else if (is_meam_spline_) {
    switch (GetComputeIndex(is_compute_energy,
                            is_compute_forces, is_compute_particle_energy,
                            is_compute_virial, is_compute_particle_virial,
                            meam_spline_->use_regular_grid_)) {
      case 0:
        ier = MeamSplineCompute<false, false, false, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 1:
        ier = MeamSplineCompute<false, false, false, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 2:
        ier = MeamSplineCompute<false, false, false, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 3:
        ier = MeamSplineCompute<false, false, false, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 4:
        ier = MeamSplineCompute<false, false, false, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 5:
        ier = MeamSplineCompute<false, false, false, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 6:
        ier = MeamSplineCompute<false, false, false, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 7:
        ier = MeamSplineCompute<false, false, false, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 8:
        ier = MeamSplineCompute<false, false, true, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 9:
        ier = MeamSplineCompute<false, false, true, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 10:
        ier = MeamSplineCompute<false, false, true, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 11:
        ier = MeamSplineCompute<false, false, true, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 12:
        ier = MeamSplineCompute<false, false, true, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 13:
        ier = MeamSplineCompute<false, false, true, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 14:
        ier = MeamSplineCompute<false, false, true, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 15:
        ier = MeamSplineCompute<false, false, true, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 16:
        ier = MeamSplineCompute<false, true, false, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 17:
        ier = MeamSplineCompute<false, true, false, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 18:
        ier = MeamSplineCompute<false, true, false, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 19:
        ier = MeamSplineCompute<false, true, false, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 20:
        ier = MeamSplineCompute<false, true, false, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 21:
        ier = MeamSplineCompute<false, true, false, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 22:
        ier = MeamSplineCompute<false, true, false, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 23:
        ier = MeamSplineCompute<false, true, false, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 24:
        ier = MeamSplineCompute<false, true, true, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 25:
        ier = MeamSplineCompute<false, true, true, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 26:
        ier = MeamSplineCompute<false, true, true, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 27:
        ier = MeamSplineCompute<false, true, true, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 28:
        ier = MeamSplineCompute<false, true, true, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 29:
        ier = MeamSplineCompute<false, true, true, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 30:
        ier = MeamSplineCompute<false, true, true, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 31:
        ier = MeamSplineCompute<false, true, true, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 32:
        ier = MeamSplineCompute<true, false, false, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 33:
        ier = MeamSplineCompute<true, false, false, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 34:
        ier = MeamSplineCompute<true, false, false, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 35:
        ier = MeamSplineCompute<true, false, false, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 36:
        ier = MeamSplineCompute<true, false, false, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 37:
        ier = MeamSplineCompute<true, false, false, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 38:
        ier = MeamSplineCompute<true, false, false, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 39:
        ier = MeamSplineCompute<true, false, false, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 40:
        ier = MeamSplineCompute<true, false, true, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 41:
        ier = MeamSplineCompute<true, false, true, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 42:
        ier = MeamSplineCompute<true, false, true, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 43:
        ier = MeamSplineCompute<true, false, true, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 44:
        ier = MeamSplineCompute<true, false, true, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 45:
        ier = MeamSplineCompute<true, false, true, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 46:
        ier = MeamSplineCompute<true, false, true, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 47:
        ier = MeamSplineCompute<true, false, true, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 48:
        ier = MeamSplineCompute<true, true, false, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 49:
        ier = MeamSplineCompute<true, true, false, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 50:
        ier = MeamSplineCompute<true, true, false, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 51:
        ier = MeamSplineCompute<true, true, false, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 52:
        ier = MeamSplineCompute<true, true, false, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 53:
        ier = MeamSplineCompute<true, true, false, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 54:
        ier = MeamSplineCompute<true, true, false, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 55:
        ier = MeamSplineCompute<true, true, false, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 56:
        ier = MeamSplineCompute<true, true, true, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 57:
        ier = MeamSplineCompute<true, true, true, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 58:
        ier = MeamSplineCompute<true, true, true, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 59:
        ier = MeamSplineCompute<true, true, true, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 60:
        ier = MeamSplineCompute<true, true, true, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 61:
        ier = MeamSplineCompute<true, true, true, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 62:
        ier = MeamSplineCompute<true, true, true, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 63:
        ier = MeamSplineCompute<true, true, true, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      default:
        HELPER_LOG_ERROR("Unknown compute function index");
        ier = true;
        break;
    }
  } else if (is_meam_sw_spline_) {
    switch (GetComputeIndex(is_compute_energy,
                            is_compute_forces, is_compute_particle_energy,
                            is_compute_virial, is_compute_particle_virial,
                            meam_sw_spline_->use_regular_grid_)) {
      case 0:
        ier = MeamSWSplineCompute<false, false, false, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 1:
        ier = MeamSWSplineCompute<false, false, false, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 2:
        ier = MeamSWSplineCompute<false, false, false, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 3:
        ier = MeamSWSplineCompute<false, false, false, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 4:
        ier = MeamSWSplineCompute<false, false, false, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 5:
        ier = MeamSWSplineCompute<false, false, false, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 6:
        ier = MeamSWSplineCompute<false, false, false, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 7:
        ier = MeamSWSplineCompute<false, false, false, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 8:
        ier = MeamSWSplineCompute<false, false, true, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 9:
        ier = MeamSWSplineCompute<false, false, true, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 10:
        ier = MeamSWSplineCompute<false, false, true, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 11:
        ier = MeamSWSplineCompute<false, false, true, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 12:
        ier = MeamSWSplineCompute<false, false, true, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 13:
        ier = MeamSWSplineCompute<false, false, true, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 14:
        ier = MeamSWSplineCompute<false, false, true, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 15:
        ier = MeamSWSplineCompute<false, false, true, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 16:
        ier = MeamSWSplineCompute<false, true, false, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 17:
        ier = MeamSWSplineCompute<false, true, false, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 18:
        ier = MeamSWSplineCompute<false, true, false, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 19:
        ier = MeamSWSplineCompute<false, true, false, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 20:
        ier = MeamSWSplineCompute<false, true, false, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 21:
        ier = MeamSWSplineCompute<false, true, false, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 22:
        ier = MeamSWSplineCompute<false, true, false, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 23:
        ier = MeamSWSplineCompute<false, true, false, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 24:
        ier = MeamSWSplineCompute<false, true, true, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 25:
        ier = MeamSWSplineCompute<false, true, true, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 26:
        ier = MeamSWSplineCompute<false, true, true, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 27:
        ier = MeamSWSplineCompute<false, true, true, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 28:
        ier = MeamSWSplineCompute<false, true, true, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 29:
        ier = MeamSWSplineCompute<false, true, true, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 30:
        ier = MeamSWSplineCompute<false, true, true, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 31:
        ier = MeamSWSplineCompute<false, true, true, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 32:
        ier = MeamSWSplineCompute<true, false, false, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 33:
        ier = MeamSWSplineCompute<true, false, false, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 34:
        ier = MeamSWSplineCompute<true, false, false, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 35:
        ier = MeamSWSplineCompute<true, false, false, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 36:
        ier = MeamSWSplineCompute<true, false, false, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 37:
        ier = MeamSWSplineCompute<true, false, false, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 38:
        ier = MeamSWSplineCompute<true, false, false, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 39:
        ier = MeamSWSplineCompute<true, false, false, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 40:
        ier = MeamSWSplineCompute<true, false, true, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 41:
        ier = MeamSWSplineCompute<true, false, true, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 42:
        ier = MeamSWSplineCompute<true, false, true, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 43:
        ier = MeamSWSplineCompute<true, false, true, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 44:
        ier = MeamSWSplineCompute<true, false, true, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 45:
        ier = MeamSWSplineCompute<true, false, true, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 46:
        ier = MeamSWSplineCompute<true, false, true, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 47:
        ier = MeamSWSplineCompute<true, false, true, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 48:
        ier = MeamSWSplineCompute<true, true, false, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 49:
        ier = MeamSWSplineCompute<true, true, false, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 50:
        ier = MeamSWSplineCompute<true, true, false, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 51:
        ier = MeamSWSplineCompute<true, true, false, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 52:
        ier = MeamSWSplineCompute<true, true, false, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 53:
        ier = MeamSWSplineCompute<true, true, false, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 54:
        ier = MeamSWSplineCompute<true, true, false, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 55:
        ier = MeamSWSplineCompute<true, true, false, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 56:
        ier = MeamSWSplineCompute<true, true, true, false, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 57:
        ier = MeamSWSplineCompute<true, true, true, false, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 58:
        ier = MeamSWSplineCompute<true, true, true, false, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 59:
        ier = MeamSWSplineCompute<true, true, true, false, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 60:
        ier = MeamSWSplineCompute<true, true, true, true, false, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 61:
        ier = MeamSWSplineCompute<true, true, true, true, false, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 62:
        ier = MeamSWSplineCompute<true, true, true, true, true, false>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      case 63:
        ier = MeamSWSplineCompute<true, true, true, true, true, true>(
            model_compute, model_compute_arguments, particle_species_codes,
            particle_contributing, coordinates, energy, forces, particle_energy,
            *virial, particle_virial);
        break;
      default:
        HELPER_LOG_ERROR("Unknown compute function index");
        ier = true;
        break;
    }
  }

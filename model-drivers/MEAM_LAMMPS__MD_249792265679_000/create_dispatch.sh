#!/bin/sh

#
# create_dispatch.sh
#
# LGPL Version 2.1 HEADER START
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
#
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301  USA
#
# LGPL Version 2.1 HEADER END
#

#
# Copyright (c) 2019--2020, Regents of the University of Minnesota.
# All rights reserved.
#
# Contributors:
#    Ryan S. Elliott
#    Stephen M. Whalen
#    Yaser Afshar
#

file_name=meam_implementation_compute_dispatch.cpp

if test -e $file_name; then 
  rm -fr $file_name
fi

printf "  if (is_meam_c_) {\n"                                                           >> $file_name
printf "    switch (GetComputeIndex(is_compute_energy,\n"                                >> $file_name
printf "                            is_compute_forces, is_compute_particle_energy,\n"    >> $file_name
printf "                            is_compute_virial, is_compute_particle_virial)) {\n" >> $file_name

i=0
for energy in false true; do
  for force in false true; do
    for particleEnergy in false true; do
      for virial in false true; do
        for particleVirial in false true; do
          printf "      case $i:\n"                                                                   >> $file_name
          printf "        ier = MeamCCompute<$energy, $force, $particleEnergy, $virial, "             >> $file_name
          printf "$particleVirial>(\n"                                                                >> $file_name
          printf "            model_compute, model_compute_arguments, particle_species_codes,\n"      >> $file_name
          printf "            particle_contributing, coordinates, energy, forces, particle_energy,\n" >> $file_name
          printf "            *virial, particle_virial);\n"                                           >> $file_name
          printf "        break;\n"                                                                   >> $file_name
          i=`expr $i + 1`
        done  # particleVirial
      done  # virial
    done  # particleEnergy
  done  # force
done  # energy

printf "      default:\n"                                                                >> $file_name
printf "        HELPER_LOG_ERROR(\"Unknown compute function index\");\n"                 >> $file_name
printf "        ier = true;\n"                                                           >> $file_name
printf "        break;\n"                                                                >> $file_name
printf "    }\n"                                                                         >> $file_name
printf "  } else if (is_meam_spline_) {\n"                                               >> $file_name
printf "    switch (GetComputeIndex(is_compute_energy,\n"                                >> $file_name
printf "                            is_compute_forces, is_compute_particle_energy,\n"    >> $file_name
printf "                            is_compute_virial, is_compute_particle_virial,\n"    >> $file_name
printf "                            meam_spline_->use_regular_grid_)) {\n"               >> $file_name

i=0
for energy in false true; do
  for force in false true; do
    for particleEnergy in false true; do
      for virial in false true; do
        for particleVirial in false true; do
          for regularGrid in false true; do
            printf "      case $i:\n"                                                                   >> $file_name
            printf "        ier = MeamSplineCompute<$energy, $force, $particleEnergy, $virial, "        >> $file_name
            printf "$particleVirial, $regularGrid>(\n"                                                  >> $file_name
            printf "            model_compute, model_compute_arguments, particle_species_codes,\n"      >> $file_name
            printf "            particle_contributing, coordinates, energy, forces, particle_energy,\n" >> $file_name
            printf "            *virial, particle_virial);\n"                                           >> $file_name
            printf "        break;\n"                                                                   >> $file_name
            i=`expr $i + 1`
          done  # regularGrid
        done  # particleVirial
      done  # virial
    done  # particleEnergy
  done  # force
done  # energy

printf "      default:\n"                                                                >> $file_name
printf "        HELPER_LOG_ERROR(\"Unknown compute function index\");\n"                 >> $file_name
printf "        ier = true;\n"                                                           >> $file_name
printf "        break;\n"                                                                >> $file_name
printf "    }\n"                                                                         >> $file_name
printf "  } else if (is_meam_sw_spline_) {\n"                                            >> $file_name
printf "    switch (GetComputeIndex(is_compute_energy,\n"                                >> $file_name
printf "                            is_compute_forces, is_compute_particle_energy,\n"    >> $file_name
printf "                            is_compute_virial, is_compute_particle_virial,\n"    >> $file_name
printf "                            meam_sw_spline_->use_regular_grid_)) {\n"            >> $file_name

i=0
for energy in false true; do
  for force in false true; do
    for particleEnergy in false true; do
      for virial in false true; do
        for particleVirial in false true; do
          for regularGrid in false true; do
            printf "      case $i:\n"                                                                   >> $file_name
            printf "        ier = MeamSWSplineCompute<$energy, $force, $particleEnergy, $virial, "      >> $file_name
            printf "$particleVirial, $regularGrid>(\n"                                                  >> $file_name
            printf "            model_compute, model_compute_arguments, particle_species_codes,\n"      >> $file_name
            printf "            particle_contributing, coordinates, energy, forces, particle_energy,\n" >> $file_name
            printf "            *virial, particle_virial);\n"                                           >> $file_name
            printf "        break;\n"                                                                   >> $file_name
            i=`expr $i + 1`
          done  # regularGrid
        done  # particleVirial
      done  # virial
    done  # particleEnergy
  done  # force
done  # energy

printf "      default:\n"                                                >> $file_name
printf "        HELPER_LOG_ERROR(\"Unknown compute function index\");\n" >> $file_name
printf "        ier = true;\n"                                           >> $file_name
printf "        break;\n"                                                >> $file_name
printf "    }\n"                                                         >> $file_name
printf "  }\n"                                                           >> $file_name

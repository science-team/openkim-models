//
// two_body.hpp
//
// LGPL Version 2.1 HEADER START
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
//
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
// MA 02110-1301  USA
//
// LGPL Version 2.1 HEADER END
//

//
// Copyright (c) 2020, Regents of the University of Minnesota.
// All rights reserved.
//
// Contributors:
//    Yaser Afshar
//

#ifndef TWO_BODY_HPP
#define TWO_BODY_HPP

/*! \class TwoBody
 * \brief Helper data structure for \c MeamSplineCompute compute routine.
 *
 * \sa MEAMImplementation::MeamSplineCompute
 *
 */
struct TwoBody {
  /*! species index */
  int index;
  /*! index type */
  int species;
  /*! distance */
  double r;
  /*! f */
  double f;
  /*! f derivative */
  double f_prime;
  /*! distance x component */
  double dx;
  /*! distance y component */
  double dy;
  /*! distance z component */
  double dz;
};

/*! \class SWTwoBody
 * \brief Helper data structure for \c MeamSWSplineCompute compute routine.
 *
 * \sa MEAMImplementation::MeamSWSplineCompute
 */
struct SWTwoBody {
  /*! species index */
  int index;
  /*! distance */
  double r;
  /*! f */
  double f;
  /*! f derivative */
  double f_prime;
  /*! SW f */
  double sw_f;
  /*! SW f derivative */
  double sw_f_prime;
  /*! distance x component */
  double dx;
  /*! distance y component */
  double dy;
  /*! distance z component */
  double dz;
};

#endif  // TWO_BODY_HPP
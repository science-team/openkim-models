# Modified Embedded Atom Method (MEAM) potential

This directory contains the modified embedded atom method (MEAM) potential model
driver [[1,2,3,4,5,6,7,8](#references)].

## MEAM model driver

This KIM model driver is written in C++ and implements three styles of
modified embedded atom method (MEAM) potentials:

- ['meam/c'](#meamc-style)
- ['meam/spline'](#meamspline-style)
- ['meam/sw/spline'](#meamswspline-style)

The style of the potential is automatically detected based on the input
files to the driver. The input files are ASCII text files formatted to be
consistent with the other MD codes that implement MEAM potentials, such as
LAMMPS, serial DYNAMO code, and Warp.

This driver has a design to mimic and reproduce the behavior of the
[LAMMPS](https://lammps.sandia.gov)
['meam/c'](https://lammps.sandia.gov/doc/pair_meamc.html),
['meam/spline'](https://lammps.sandia.gov/doc/pair_meam_spline.html), and
['meam/sw/spline'](https://lammps.sandia.gov/doc/pair_meam_sw_spline.html) pair
styles.\
We adapted the LAMMPS format from the
[LAMMPS documentation](https://lammps.sandia.gov/doc/) as of `Oct 15, 2020`.

For any of the three styles mentioned above, the driver expects an element file.
Depending on the specific potential style, other files may be required/supplied
and will be outlined in their respective sections below.

### Element file format

The element file contains a unique list of chemical element names separated
by spaces:

```bash
     Elem1 Elem2 Elem3 ...
```

For example, for an Si and C alloy system the element file may look like this:

```bash
     Si C
```

For an alloy system containing Al, Si, Mg, Cu, and Fe, it could look like this:

```bash
     Al Si Mg Cu Fe
```

**NOTE:**

There is no limit on the maximum number of elements in the element file.

**NOTE:**

The order in which elements are listed in the element file defines the
integral indices used to identify each element in the [MEAM parameter
file](#meam-parameter-file-format) (if you choose to specify one).
Therefore, care must be taken to ensure that the indices used to assign
parameter values in the MEAM parameter file are consistent with the order of
the elements in the element file. (See details
[below](#meam-parameter-file-format) on the syntax for settings in the
'meam/c' parameter file.)

## MEAM/C style

In the 'meam/c' style, the driver computes pairwise interactions for a variety
of materials using modified embedded atom method (MEAM) potentials
[[1,2,3,4,5,6,7](#references)].
Conceptually, it is an extension to the original EAM potentials that adds
angular forces. It is thus suitable for modeling metals and alloys with `fcc`,
`bcc`, `hcp` and `diamond` cubic structures, as well as covalently bonded
materials like silicon and carbon.

In the MEAM formulation, the total energy Ei of an atom I is given by:

<img src="https://latex.codecogs.com/svg.latex?E_i%20=%20U_\alpha%20(\Bar{\rho}_i)+\frac{1}{2}\sum\limits_{j\ne%20i}%20\phi_{\alpha%20\beta}(r_{ij}),"/>

```tex
     E_i = U_\alpha (\Bar{\rho}_i)
         + \frac{1}{2}\sum\limits_{j \ne i} \phi_{\alpha \beta}(r_{ij}),
```

where `U` is the embedding energy which is a function of the atomic electron
density `rho bar`, and `phi` is a pair potential interaction. The pair
interaction is summed over all neighbors J of atom I within the cutoff distance.

As with EAM, the multi-body nature of the MEAM potential is a result of the
embedding energy term. Details of the computation of the embedding and pair
energies, as implemented in this model driver, are given in
[[9](#references)] and references therein.

The various parameters in the MEAM formulas are listed in two files.

1) The MEAM library file, usually `library.meam`.
2) The MEAM parameter file (alloy parameter file). The name usually contains the
   elements' names and ends in the `.meam` extension. For example, for an
   alloy containing Si and C, the `SiC.meam` file (alloy parameter file)
   contains the specific parameter settings.

### MEAM library file format

This file has generic MEAM settings for a variety of elements. The MEAM library
file is an input file by some MD codes (e.g., LAMMPS, serial DYNAMO code, and
Warp). Aside from blank and comment lines (which start with #), which can
appear anywhere, its format is a series of entries, each of which has 19
parameters and can span multiple lines [[11](#references)].

```txt
     elt, lat, z, ielement, atwt, alpha, b0, b1, b2, b3, alat, esub, asub, t0, t1, t2, t3, rozero, ibar
```

The `elt` and `lat` parameters are text strings, such as `elt = Si` or
`elt = Cu` and `lat = bcc, dia, ... or fcc`. Because Fortran MD codes use the
library file, these strings may include single quotes [[11](#references)].

To choose and identify the library file's proper settings, we use the value of
the `elt` string. Since there can be multiple entries in the library file with
the same `elt` value, we follow the LAMMPS code [[10](#references)], reading
the first matching entry and ignoring the rest.

In the MEAM library file, the parameters
`lat, z, ielement, atwt, alat, esub, asub` correspond to single-element
potential parameters. The meaning of these paramters is as follows:

```txt
     lat      = lattice structure of reference configuration
     z        = number of nearest neighbors in the reference structure
     ielement = atomic number
     atwt     = atomic weight
     alat     = lattice constant of reference structure
     esub     = energy per atom (eV) in the reference structure at equilibrium
     asub     = "A" parameter for the MEAM (see e.g., [1,2])
```

The `alpha, b0, b1, b2, b3, t0, t1, t2, t3` parameters correspond to the
literature's standard MEAM parameters [[1,2](#references)] (the `b*`
parameters are the standard beta parameters).
Note that only parameters normalized to `t0 = 1.0` are supported.
The `rozero` parameter is an element-dependent density scaling that weights the
reference background density (see, e.g., equation 4.5 in [[9](#references)]) and
is typically 1.0 for single-element systems. The `ibar` parameter selects the
form of the function `G(Gamma)` used to compute the electron density. Different
values of the `ibar` parameter correspond to the following forms:

```txt
      0 => G = sqrt(1+Gamma)
      1 => G = exp(Gamma/2)
      2 => not implemented
      3 => G = 2/(1+exp(-Gamma))
      4 => G = sqrt(1+Gamma)
     -5 => G = +-sqrt(abs(1+Gamma))
```

An example MEAM library file for an SiC alloy system is shown below:

```bash
     # DATE: 2012-06-29 UNITS: metal DATE: 2007-06-11
     # CONTRIBUTOR: Greg Wagner, gjwagne@sandia.gov
     # CITATION: Baskes, Phys Rev B, 46, 2727-2742 (1992)
     # meam data from vax files fcc,bcc,dia    11/4/92
     # elt        lat     z       ielement     atwt
     # alpha      b0      b1      b2           b3    alat    esub    asub
     # t0         t1              t2           t3            rozero  ibar
     'Si'        'dia'   4.      14           28.086
     4.87        4.8     4.8     4.8          4.8   5.431   4.63    1.
     1.0         3.30            5.105        -0.80         1.      1
     'C'         'dia'   4.      6            12.0111
     4.38        4.10    4.200   5.00         3.00  3.567   7.37    1.000
     1.0         5.0             9.34         -1.00         2.25    1
```

**NOTE:**

There is no limit on the maximum number of elements that can be read from the
MEAM library file.

### MEAM parameter file format

The MEAM parameter file is an optional file containing settings that
override or complement the library file settings. The format is the same as
is read by other Fortran MD codes. Aside from blank and comment lines (start
with `#`), which can appear anywhere, each line has one of the following
forms:

```txt
     keyword = value
     keyword(I) = value
     keyword(I,J) = value
     keyword(I,J,K) = value
```

The driver also ignores anything after a trailing comment within a line
(starting with `#`).

The indices I, J, K correspond to the elements listed in the [MEAM element
file](#element-file-format) in the order in which they appear. These indices
start from 1 to be compatible with LAMMPS and the other Fortran MD codes.
For example, if one were modeling an alloy containing Al, Si, Mg, Cu, and
Fe, and used a MEAM element file containing the following:

```bash
     Al Si Mg Cu Fe
```

then an index of 1 would refer to Al, an index of 2 to Si, an index of 3 to
Mg, an index of 4 to Cu, and an index of 5 to Fe.

The recognized keywords for the parameter file are as follows:

```txt
     rc              = cutoff radius for cutoff function. (default = 4.0)
     delr            = length of smoothing distance for cutoff function.
                       (default = 0.1)
     rho0(I)         = relative density for element I
     Ec(I,J)         = cohesive energy of reference structure for I-J mixture
     delta(I,J)      = heat of formation for I-J alloy. If Ec_IJ is input as
                       zero, then driver sets
                       Ec_IJ = (Ec_II + Ec_JJ)/2 - delta_IJ
     alpha(I,J)      = alpha parameter for pair potential between I and J (can
                       be computed from bulk modulus of reference structure
     re(I,J)         = equilibrium distance between I and J in the reference
                       structure
     Cmax(I,J,K)     = Cmax screening parameter when I-J pair is screened
                       by K (I<=J); default = 2.8
     Cmin(I,J,K)     = Cmin screening parameter when I-J pair is screened
                       by K (I<=J); default = 2.0
     lattce(I,J)     = lattice structure of I-J reference structure:
                       fcc  = face centered cubic
                       bcc  = body centered cubic
                       hcp  = hexagonal close-packed
                       dim  = dimer
                       dia  = diamond (interlaced fcc for alloy)
                       dia3 = diamond structure with primary 1NN and secondary
                              3NN interaction
                       b1   = rock salt (NaCl structure)
                       c11  = MoSi2 structure
                       l12  = Cu3Au structure (lower case L, followed by 12)
                       b2   = CsCl structure (interpenetrating simple cubic)
                       ch4  = methane-like structure, only for binary system
                       lin  = linear structure (180 degree angle)
                       zig  = zigzag structure with a uniform angle
                       tri  = H2O-like structure that has an angle
     nn2(I,J)        = turn on second-nearest neighbor MEAM formulation for I-J
                       pair (see for example []()). (default = 0)
                       0 = second-nearest neighbor formulation off
                       1 = second-nearest neighbor formulation on
     attrac(I,J)     = additional cubic attraction term in Rose energy I-J pair
                       potential (default = 0)
     repuls(I,J)     = additional cubic repulsive term in Rose energy I-J pair
                       potential (default = 0)
     zbl(I,J)        = blend the MEAM I-J pair potential with the ZBL potential
                       for small atom separations (ZBL) (default = 1)
     theta(I,J)      = angle between three atoms in line, zigzag, and trimer
                       reference structures in degrees (default = 180)
     gsmooth_factor  = factor determining the length of the G-function smoothing
                       region. (default = 99.0)
                       Only significant for ibar=0 or ibar=4.
                       99.0 = short smoothing region, sharp step
                       0.5  = long smoothing region, smooth step
     augt1           = integer flag for whether to augment t1 parameter by
                       3/5*t3 to account for old vs. new meam formulations,
                       (default = 1)
                       0 = don't augment t1
                       1 = augment t1
     ialloy          = integer flag to use alternative averaging rule for t
                       parameters, for comparison with the DYNAMO MEAM code
                       (default = 0)
                       0 = standard averaging (matches ialloy=0 in DYNAMO)
                       1 = alternative averaging (matches ialloy=1 in DYNAMO)
                       2 = no averaging of t (use single-element values)
     mixture_ref_t   = integer flag to use mixture average of t to compute the
                       background reference density for alloys, instead of the
                       single-element values (see description and warning
                       elsewhere in this doc page) (default = 0)
                       0 = do not use mixture averaging for t in the reference
                           density
                       1 = use mixture averaging for t in the reference density
     erose_form      = integer value to select the form of the Rose energy
                       function (see description below) (default = 0).
     emb_lin_neg     = integer value to select embedding function for negative
                       densities (default = 0)
                       0 = F(rho)=0
                       1 = F(rho) = -asub*esub*rho (linear in rho, matches
                           DYNAMO)
     bkgd_dyn          = integer value to select background density formula
                         (default = 0)
                       0 = rho_bkgd = rho_ref_meam(a) (as in the reference
                           structure)
                       1 = rho_bkgd = rho0_meam(a)*Z_meam(a) (matches DYNAMO)
```

`Rc, delr, re` are in distance units (`Angstroms` in the case of metal units).
`Ec` and `delta` are in the energy units (`eV` in the case of metal units).

Each keyword represents a quantity which is either a scalar, vector, 2d array,
or 3d array and must be specified with the correct corresponding array syntax.
The indices I,J,K each run from 1 to N where N is the number of MEAM elements
being used.

Thus, these lines,

```txt
     rho0(4) = 3.0
     alpha(1,5) = 4.2
```

set `rho0` for the fourth element to the value 3.0 and set the `alpha` for
the cross-species interaction between elements 1 and 5 to 4.2.

The `augt1` parameter is related to modifications in the MEAM formulation of the
partial electron density function. The augmentation is the default option. But
when the parameter values are fit using the modified density function, `augt1`
is 0.

The `mixture_ref_t` parameter is related to the background reference density
computation. Setting `mixture_ref_t=1` causes the driver to use an alloy mixture
averaged value of t to compute the background reference density. The proper way
to calculate the background reference density is to use the single-element
values of the t parameters.

**NOTE:**

One should avoid using `mixture_ref_t=1` whenever possible [[11](#references)].
Using `mixture_ref_t=1` will give incorrect results for the second-neighbor
MEAM, and it is non-standard for the first-neighbor MEAM.

The parameters `attrac` and `repuls,` along with the integer selection parameter
`erose_form,` can be used to modify the Rose energy function used to compute the
pair potential. This function gives the energy of the reference state as a
function of interatomic spacing. The form of this function is:

<img src="https://latex.codecogs.com/svg.latex?\begin{align}%20\nonumber%20&a^*%20=%20\alpha%20(\frac{r}{r_e}%20-%201)\\%20\nonumber%20\\%20\nonumber%20&\text{form}=0\\%20\nonumber%20&E=\left\{\begin{matrix}-E_c\left(1+a^*+%20{a_\text{repuls}}~%20\frac{{a^*}^3}{r/re}%20\right)exp(-a^*)&%20a^*%20<%200\\%20-E_c\left(1+a^*+%20{a_\text{attrac}}~%20\frac{{a^*}^3}{r/re}\right)exp(-a^*)%20&%20a^*%20\geqslant%200%20\end{matrix}\right.%20\\\nonumber\\%20\nonumber%20&\text{form}=1\\%20\nonumber%20&E=-E_c\left(1+a^*+%20\left(-{a_\text{attrac}}~+\frac{{a_\text{repuls}}}{r}\right){a^*}^3%20\right)exp(-a^*)%20\\%20\nonumber%20\\%20\nonumber%20&\text{form}=2\\%20\nonumber%20&E=\left\{\begin{matrix}-E_c\left(1+a^*+%20{a_\text{repuls}}~%20{a^*}^3%20\right)exp(-a^*)%20&%20a^*%20<%200\\-E_c\left(1+a^*+%20{a_\text{attrac}}~%20{a^*}^3%20\right)exp(-a^*)%20&%20a^*\geqslant%200%20\end{matrix}\right.%20\end{align}"/>

```tex
     a^* = \alpha (\frac{r}{r_e} - 1)

     form=0

     E=\left\{\begin{matrix}
     -E_c\left(1+a^*+ {a_\text{repuls}}~ \frac{{a^*}^3}{r/re} \right)exp(-a^*)
     & a^* < 0\\ -E_c\left(1+a^*+ {a_\text{attrac}}~ \frac{{a^*}^3}{r/re}
     \right)exp(-a^*) & a^* \geqslant 0 \end{matrix}\right.

     form=1

     E=-E_c\left(1+a^*+ \left(-{a_\text{attrac}}~+
     \frac{{a_\text{repuls}}}{r}\right){a^*}^3 \right)exp(-a^*)

     form=2

     E=\left\{\begin{matrix}
     -E_c\left(1+a^*+ {a_\text{repuls}}~ {a^*}^3 \right)exp(-a^*) & a^* < 0\\
     -E_c\left(1+a^*+ {a_\text{attrac}}~ {a^*}^3 \right)exp(-a^*) & a^*
     \geqslant 0 \end{matrix}\right.
```

For example, below is an example MEAM parameter file for a SiC alloy system:

```bash
     # DATE: 2007-06-11 UNITS: metal CONTRIBUTOR: Greg Wagner, gjwagne@sandia.gov CITATION: Unknown
     lattce(1,2) = 'dia'
     Ec(1,2) = 6.4325
     alpha(1,2) = 4.37
     re(1,2) = 1.8878
     rho0(2) = 2.25
     rc = 4.0
     delr = 0.1
     Cmax(1,2,1) = 4.0
     Cmax(1,2,2) = 4.0
     Cmax(2,2,1) = 4.0
     Cmax(1,1,2) = 4.0
```

### MEAM/C style example

As an example, a KIM Portable Model (PM) for pure `Al` systems that uses
this model driver with the `meam/c` style might contain the following three
input files:

```bash
     elems.meam
     library.meam
     Al.meam
```

where `elems.meam` element file would contain

```bash
     # list of elements
     # Elem1, Elem2
     Al
```

the `library.meam` library file might consist of

```bash
     # References:
     # elt        lat     z       ielement     atwt
     # alpha      b0      b1      b2           b3    alat    esub    asub
     # t0         t1              t2           t3            rozero  ibar
     #
     'Al'        'fcc'   12.     13           26.9815
     4.68604   1.56205   5.39270   5.29601  -1.00047   4.05000   3.35999   1.06859
     1.0  -1.54917  -1.28508  10.01041 1.0 0
```

and the `Al.meam` MEAM parameter file could contain

```bash
     re(1,1) =   2.86378
     attrac(1,1) = 0.39558
     repuls(1,1) = 0.09806
     Cmin(1,1,1) = 1.00769
     Cmax(1,1,1) = 2.31407
     rc = 5.0
     delr = 0.1
     augt1 = 1
```

## MEAM/SPLINE style

The `meam/spline` style supported by this model driver stems from code
primarily intended for modeling metals [[8,16](#references)].

This driver supports both a single species (also called "old-style" MEAM) and
multi-component species (also called "new-style" MEAM). In this style, the atomic
electron density is,

<img src="https://latex.codecogs.com/svg.latex?\Bar{\rho}_i=\sum\limits_{j%20\ne%20i}\rho_j(r_{ij})%20+%20\sum\limits_{\substack{j%20<%20k,%20\\%20j%20\ne%20i}}%20f_j(r_{ij})%20f_k(r_{ik})%20g_{jk}{\left(\cos(\theta_{jik})\right)},"/>

```tex
     \Bar{\rho}_i=\sum\limits_{j \ne i}\rho_j(r_{ij}) + \sum\limits_{\substack{j < k, \\ j \ne i}} f_j(r_{ij}) f_k(r_{ik}) g_{jk}{\left(\cos(\theta_{jik})\right)},
```

where `rho` is the density, and `theta` is the angle between atoms. The five
functions `phi`, `U`, `rho`, `f`, and `g` depend on the atoms in the
interaction. Specifically, if there are N species, then there are N
different functions `U`, `rho`, and `f`, and there are N(N+1)/2 functions
`phi` and `g`. Each of these functions is tabulated as a cubic spline.

While the list of elements is in the element file, the potential file indicates
the cutoffs and the coefficients for these spline functions.

The MEAM potential file name usually contains the elements' names and ends in
the `.meam.spline` extension. For example, for an SiC alloy system, the
`SiC.meam.spline` file contains all of the specific parameter settings.

### MEAM potential file format

In the new-style potential file, the second line begins with the string
`meam/spline` followed by the number of unique species and their names. The
names of the elements and the order in which they appear should match the
MEAM element file.

### MEAM/SPLINE style example

As an example, a KIM Portable Model (PM) for pure `Si` systems that uses
this model driver with the `meam/spline` style might contain the following two
input files:

```bash
     elems.meam
     Si.meam.spline
```

where `elems.meam` element file would contain

```bash
     # list of elements
     # Elem1, Elem2
     Si
```

and the `Si.meam.spline` potential file could contain

```bash
     Spline-based MEAM potential for Si. Reference: T. J. Lenosky, B. Sadigh, E. Alonso, V. V. Bulatov, T. D. de la Rubia, J. Kim, A. F. Voter, and J. D. Kress, Modell. Simul. Mater. Sci. Eng. 8, 825 (2000)
     10
     -4.266966781858503300e+01 0.000000000000000000e+00
     1 0 1 0
     1.500000000000000000e+00 6.929943430771341000e+00 1.653321602557917600e+02
     1.833333333333333300e+00 -4.399503747408950400e-01 3.941543472528634600e+01
     2.166666666666666500e+00 -1.701233725061446700e+00 6.871065423413908100e+00
     2.500000000000000000e+00 -1.624732919215791800e+00 5.340648014033163800e+00
     2.833333333333333000e+00 -9.969641728342462100e-01 1.534811309391571000e+00
     3.166666666666667000e+00 -2.739141845072665100e-01 -6.334706186546093900e+00
     3.500000000000000000e+00 -2.499156963774082700e-02 -1.798864729909626500e+00
     3.833333333333333500e+00 -1.784331481529976400e-02 4.743496636420091500e-01
     4.166666666666666100e+00 -9.612303290166881000e-03 -4.006506271304824400e-02
     4.500000000000000000e+00 0.000000000000000000e+00 -2.394996574779807200e-01
     11
     -1.000000000000000000e+00 0.000000000000000000e+00
     1 0 0 0
     1.500000000000000000e+00 1.374674212682983900e-01 -3.227795813279568500e+00
     1.700000000000000000e+00 -1.483141815327918000e-01 -6.411648793604404900e+00
     1.899999999999999900e+00 -5.597204896096039700e-01 1.003068519633888300e+01
     2.100000000000000100e+00 -7.310964379372824100e-01 2.293461970618954700e+00
     2.299999999999999800e+00 -7.628287071954063000e-01 1.742018781618444500e+00
     2.500000000000000000e+00 -7.291769685066557000e-01 5.460640949384478700e-01
     2.700000000000000200e+00 -6.662022220044453400e-01 4.721760106467195500e-01
     2.899999999999999900e+00 -5.732830582550895200e-01 2.056894449546524200e+00
     3.100000000000000100e+00 -4.069014309729406300e-01 2.319615721086100800e+00
     3.299999999999999800e+00 -1.666155295956388300e-01 -2.497162196179187900e-01
     3.500000000000000000e+00 0.000000000000000000e+00 -1.237130660986393100e+01
     8
     7.351364478015182100e-01 6.165217237728655200e-01
     1 1 1 1
     -1.770934559908718700e+00 -1.074925682941420000e+00 -1.482768170233858500e-01
     -3.881557649503457600e-01 -2.004503493658201000e-01 -1.492100354067345500e-01
     9.946230300080272100e-01 4.142241371345077300e-01 -7.012475119623896900e-02
     2.377401824966400000e+00 8.793892953828742500e-01 -3.944355024164965900e-02
     3.760180619924772900e+00 1.266888024536562100e+00 -1.581431192239436000e-02
     5.142959414883146800e+00 1.629979548834614900e+00 2.611224310900800400e-02
     6.525738209841518900e+00 1.977379549636293600e+00 -1.378738550324104500e-01
     7.908517004799891800e+00 2.396177220616657200e+00 7.494253977092666400e-01
     10
     -3.618936018538757300e+00 0.000000000000000000e+00
     1 0 1 0
     1.500000000000000000e+00 1.250311510312851300e+00 2.790400588857243500e+01
     1.722222222222222300e+00 8.682060369372680600e-01 -4.522554291731776900e+00
     1.944444444444444400e+00 6.084604017544847900e-01 5.052931618779816800e+00
     2.166666666666666500e+00 4.875624808097850400e-01 1.180825096539679600e+00
     2.388888888888888800e+00 4.416345603457190700e-01 -6.673769465415171400e-01
     2.611111111111111200e+00 3.760976313325982700e-01 -8.938118490837722000e-01
     2.833333333333333000e+00 2.714524157414608400e-01 -5.090324763524399800e-01
     3.055555555555555400e+00 1.481440300150710900e-01 6.623665830603995300e-01
     3.277777777777777700e+00 4.854596610856590900e-02 7.403702452268122700e-01
     3.500000000000000000e+00 0.000000000000000000e+00 2.578982318481970500e+00
     8
     -1.395041572145673000e+01 1.134616739799360700e+00
     1 1 1 1
     -1.000000000000000900e+00 5.254163992149617700e+00 1.582685381253900500e+01
     -7.428367052748285900e-01 2.359149452448745100e+00 3.117611233789983400e+01
     -4.856734105496561800e-01 1.195946960915646100e+00 1.658962813584905800e+01
     -2.285101158244838800e-01 1.229952028074150000e+00 1.108360928564026400e+01
     2.865317890068852500e-02 2.035650777568434500e+00 9.088861456447702400e+00
     2.858164736258610400e-01 3.424741418405580000e+00 5.489943377538379500e+00
     5.429797683510331200e-01 4.948585892304984100e+00 -1.882291580187675700e+01
     8.001430630762056400e-01 5.617988713941801200e+00 -7.718625571850646200e+00
```

## MEAM/SW/SPLINE style

The `meam/sw/spline` style supported by the driver is similar to the
`meam/spline` style except for an additional Stillinger-Weber (SW) term
that's added onto the energy.

For this MEAM style, only a single species is supported. The total energy Ei
of an atom I is given by,

<img src="https://latex.codecogs.com/svg.latex?E_i%20=%20U_\alpha%20(\Bar{\rho}_i)%20+%20\frac{1}{2}\sum\limits_{j%20\ne%20i}%20\phi_{\alpha%20\beta}(r_{ij})%20+%20{E_{SW}}_i,"/>

```tex
     E_i = U_\alpha (\Bar{\rho}_i)
         + \frac{1}{2}\sum\limits_{j \ne i} \phi_{\alpha \beta}(r_{ij}) + {E_{SW}}_i,
```

where,

<img src="https://latex.codecogs.com/svg.latex?{E_{SW}}_i%20=%20\sum\limits_{\substack{j%20<%20k,%20\\%20j%20\ne%20i}}%20F_j(r_{ij})%20F_k(r_{ik})%20G_{jk}{\left(\cos(\theta_{jik})\right)}."/>

```tex
     {E_{SW}}_i = \sum\limits_{\substack{j < k, \\ j \ne i}} F_j(r_{ij}) F_k(r_{ik}) G_{jk}{\left(\cos(\theta_{jik})\right)}.
```

The atomic electron density is,

<img src="https://latex.codecogs.com/svg.latex?\Bar{\rho}_i=\sum\limits_{j%20\ne%20i}\rho_j(r_{ij})%20+%20\sum\limits_{\substack{j%20<%20k,%20\\%20j%20\ne%20i}}%20f_j(r_{ij})%20f_k(r_{ik})%20g_{jk}{\left(\cos(\theta_{jik})\right)},"/>

```tex
     \Bar{\rho}_i=\sum\limits_{j \ne i}\rho_j(r_{ij}) + \sum\limits_{\substack{j < k, \\ j \ne i}} f_j(r_{ij}) f_k(r_{ik}) g_{jk}{\left(\cos(\theta_{jik})\right)},
```

where `rho` is the density, and `theta` is the angle between atoms. The seven
functions `phi`, `U`, `F`, `G`, `rho`, `f`, and `g`, are represented by cubic
splines.

While the species name is in the element file, the potential file indicates
the cutoffs and the coefficients for these spline functions.

The MEAM potential file name usually contains the element's name and ends in
the `.meam.sw.spline` extension.

For example, for an Ti system, the `Ti.meam.sw.spline` file contains the
specific parameter settings.

### MEAM/SW/SPLINE style example

As an example, a KIM Portable Model (PM) for pure `Ti` system that uses this
model driver with the `meam/sw/spline` style might contain the following two
input files:

```bash
     elems.meam
     Ti.meam.sw.spline
```

where `elems.meam` element file would contain

```bash
     # list of elements
     # Elem1, Elem2
     Ti
```

and the `Ti.meam.sw.spline` potential file might contain

```bash
     # DATE: 2012-02-01 UNITS: metal CONTRIBUTOR: Robert Rudd, robert.rudd@llnl.gov CITATION: Hennig, Lenosky, Trinkle, Rudin, and Wilkins, Phys Rev B, 78, 054121 (2008) COMMENT: Spline-based MEAM+SW potential for Ti (R.G. Hennig et al., Phys. Rev. B 78, 054121 (2008)).
     13
     -20.0 0.0
     1 0 1 0
     1.742692837163632546e+00 3.744277175966389349e+00 0.0e+00
     2.055801767399996649e+00 9.108397309055012991e-01 1.087025233010940539e+01
     2.368910697636360752e+00 3.880458966341450711e-01 -1.553224188359566327e+00
     2.682019627872724410e+00 -1.884090653346989427e-02 2.436300413714785229e+00
     2.995128558109088512e+00 -2.480989296390785637e-01 2.679127139950259640e+00
     3.308237488345452615e+00 -2.644895502965277645e-01 -1.250563846088559861e-01
     3.621346418581816273e+00 -2.271961892825972718e-01 1.106625553693794339e+00
     3.934455348818180376e+00 -1.292930901760148965e-01 -5.920536767960294933e-01
     4.247564279054544478e+00 -5.968536693320936753e-02 -4.701234146338039155e-01
     4.560673209290908581e+00 -3.110002556084055444e-02 -3.807399733552661869e-02
     4.873782139527271795e+00 -1.384736320244591042e-02 -7.115479604519592272e-02
     5.186891069763635898e+00 -3.203412727856397215e-03 -8.176829245009833991e-02
     5.500000000000000000e+00 0.000000000000000000e+00 -5.714229647754404118e-02
     2
     0.000000000000000000e+00 0.000000000000000000e+00
     1 0 0 0
     2.055801767399996649e+00 0.000000000000000000e+00 0.000000000000000000e+00
     4.410000000000000142e+00 0.000000000000000000e+00 0.000000000000000000e+00
     2
     0.000000000000000000e+00 0.000000000000000000e+00
     1 0 0 0
     -1.000000000000000000e+00 0.000000000000000000e+00 0.000000000000000000e+00
     9.284366193603559303e-01  0.000000000000000000e+00 0.000000000000000000e+00
     11
     -1.000000000000000000e+00 0.000000000000000000e+00
     1 0 0 0
     2.055801767399996649e+00 1.747527966078008976e+00 -5.258697870855910423e+02
     2.291221590659997087e+00 -5.867796394513962177e+00 2.527963170143783884e+02
     2.526641413919997525e+00 -8.337628873718182732e+00 7.173183889436359095e+01
     2.762061237179997519e+00 -5.839871284245343297e+00 -1.935877422943027337e+00
     2.997481060439997957e+00 -3.114064823053663389e+00 -3.929991928613325314e+01
     3.232900883699998396e+00 -1.725724506500649014e+00 1.434241360892369954e+01
     3.468320706959998834e+00 -4.428977016930342181e-01 -2.949255346943084177e+01
     3.703740530219999272e+00 -1.466643003340316054e-01 -3.180105342079309949e+00
     3.939160353479999266e+00 -2.095507944774975262e-01 3.334908384292152750e+00
     4.174580176740000148e+00 -1.442384563223014227e-01 3.719186918672943154e+00
     4.410000000000000142e+00 0.000000000000000000e+00 -9.667170204974903314e+00
     4
     7.769349460453946719e-03 1.051977061603435737e-01
     1 1 1 1
     -5.514233164927543385e+01 -2.974556800785587707e-01 1.528706039584923782e-03
     -4.474098990370920603e+01 -1.544945872174040002e-01 3.893372250824005966e-04
     -3.433964815814297822e+01 5.098657168053199323e-02 3.812492703804592413e-04
     -2.393830641257674685e+01 5.734269470406565539e-01 1.566392648865140222e-02
     10
     2.773275116566607856e+00 0.000000000000000000e+00
     1 0 1 0
     2.055801767399996649e+00 -1.485215264177146000e-01 7.220108674930609993e+01
     2.317379348799997185e+00 1.684530491819494546e+00 -4.727446892749223650e+01
     2.578956930199997277e+00 2.011336597660079661e+00 -1.518595783989647252e+01
     2.840534511599997813e+00 1.144409274729116577e+00 3.339782043718398263e+00
     3.102112092999998350e+00 2.861606802608402389e-01 2.587867610190767831e+00
     3.363689674399998442e+00 -3.459281125712435068e-01 6.140706938696685491e+00
     3.625267255799998978e+00 -6.257480601030769307e-01 3.739769675678216831e+00
     3.886844837199999514e+00 -6.119510825772711549e-01 4.647490848815326814e+00
     4.148422418600000050e+00 -3.112059650865105498e-01 2.832757465268814556e+00
     4.410000000000000142e+00 0.000000000000000000e+00 -1.506120868737344587e+01
     8
     8.336422748105723812e+00 -6.040245747365641193e+01
     1 1 1 1
     -1.000000000000001998e+00 7.651409192994791664e-02 -1.106523212957412881e+02
     -7.245090543770936753e-01 1.415582454149486025e-01 4.488534055085228403e+01
     -4.490181087541854632e-01 7.578869734074348274e-01 -2.530651153464005532e+01
     -1.735271631312771679e-01 6.301157037831857100e-01 -2.485101449562865383e+00
     1.019637824916310997e-01 9.049597305054708773e-02 2.687693869168511718e+00
     3.774547281145393951e-01 -3.574158665670761348e-01 -1.015585700534352709e+00
     6.529456737374476072e-01 -6.529321764728414079e-01 1.342247859683878808e+01
     9.284366193603559303e-01 -6.009121906529053980e+00 -4.527525426982541035e+02
```

## References

[1]  M.I., Baskes, J.S., Nelson, and A.F., Wright, "Semiempirical modified
     embedded atom potentials for silicon and germanium," Phys. Rev.B,
     40:6085--6100, Sep 1989.

[2]  M.I., Baskes, "Modified embedded atom potentials for cubic materials
     and impurities," Phys. Rev.B, 46:2727--2742, Aug 1992.

[3]  M.I., Baskes, "Determination of modified embedded atom method parameters
     for nickel," Materials Chemistry and Physics, 50(2):152--158, 1997.

[4]  B.J., Lee, and M.I., Baskes, "Second nearest-neighbor modified embedded
     atom method potential," Phys. Rev.B, 62:8564, Oct 2000.

[5]  B.J., Lee, M.I., Baskes, H., Kim, and Y.K., Cho, "Second nearest-neighbor
     modified embedded atom method potentials for bcc transition metals,"
     Phys. Rev.B, 64:184102, Oct 2001.

[6]  S.M., Valone, M.I., Baskes, and R.L., Martin, "Atomistic model of helium
     bubbles in gallium-stabilized plutonium alloys," Phys. Rev.B 73:214209 June
     2006.

[7]  H., Huang, N.M., Ghoniem, J.K., Wong, and M., Baskes, "Molecular dynamics
     determination of defect energetics in beta-SiC using three representative
     empirical potentials," Modelling and Simulation in Materials Science and
     Engineering, 3(5):615--627, Sep 1995.

[8]  T.J., Lenosky, B., Sadigh, E., Alonso, V.V., Bulatov, T.D., Rubia, J., Kim,
     A.F., Voter, and J.D., Kress, "Highly optimized empirical potential model
     of silicon," Modelling and Simulation in Materials Science and Engineering,
     8(6):825--841, Oct 2000.

[9]  H., Fang, P.M., Gullett, A., Slepoy, M.F., Horstemeyer, M., Baskes,
     G.J., Wagner, and M., Li, "Numerical tools for atomistic simulations,"
     Report 918395, Sandia National Laboratories, Jan 2004.

[10] [https://lammps.sandia.gov](https://lammps.sandia.gov)

[11] [https://docs.lammps.org/pair_meamc.html](https://docs.lammps.org/pair_meamc.html)

[12] [https://docs.lammps.org/pair_meam_spline.html](https://docs.lammps.org/pair_meam_spline.html)

[13] [https://docs.lammps.org/pair_meam_sw_spline.html](https://docs.lammps.org/pair_meam_sw_spline.html)

[14]  B.J., Thijsse, "Relationship between the modified embedded atom method and
      stillinger-weber potentials in calculating the structure of silicon,"
      Phys. Rev. B, 65:195207, May 2002.

[15]  B., Jelinek, S., Groh, M.F., Horstemeyer, J., Houze, S.G., Kim, G.J.,
      Wagner, A., Moitra, and M.I., Baskes, "Modified embedded atom method
      potential for Al, Si, Mg, Cu, and Fe alloys," Phys. Rev. B, 85:245102,
      Jun 2012.

[16]  M.R., Fellinger, "First principles-based interatomic potentials for
      modeling the body-bentered cubic metals V, Nb, Ta, Mo, and W," Ph.D.
      thesis, The Ohio State University, Jan 2013.

[17]  P., Zhang and D.R., Trinkle, "A modified embedded atom method potential
      for interstitial oxygen in titanium," Computational Materials Science,
      124:204--210, Nov 2016.

## License

[LGPLv2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html)

## Copyright

```txt
LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
http://lammps.sandia.gov, Sandia National Laboratories
Steve Plimpton, sjplimp@sandia.gov
Copyright (2003) Sandia Corporation.  Under the terms of Contract
DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
certain rights in this software.  This software is distributed under
the GNU General Public License.
```

```txt
Copyright (2011) Lawrence Livermore National Security, LLC.
Produced at the Lawrence Livermore National Laboratory.
Written by Alexander Stukowski (<alex@stukowski.com>).
LLNL-CODE-525797 All rights reserved.

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License (as published by the Free
Software Foundation) version 2, dated June 1991.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the terms and conditions of the
GNU General Public License for more details.

Our Preamble Notice
A. This notice is required to be provided under our contract with the
U.S. Department of Energy (DOE). This work was produced at the
Lawrence Livermore National Laboratory under Contract No.
DE-AC52-07NA27344 with the DOE.

B. Neither the United States Government nor Lawrence Livermore National
Security, LLC nor any of their employees, makes any warranty, express or
implied, or assumes any liability or responsibility for the accuracy,
completeness, or usefulness of any information, apparatus, product, or
process disclosed, or represents that its use would not infringe
privately-owned rights.

C. Also, reference herein to any specific commercial products, process,
or services by trade name, trademark, manufacturer or otherwise does not
necessarily constitute or imply its endorsement, recommendation, or
favoring by the United States Government or Lawrence Livermore National
Security, LLC. The views and opinions of authors expressed herein do not
necessarily state or reflect those of the United States Government or
Lawrence Livermore National Security, LLC, and shall not be used for
advertising or product endorsement purposes.
```

```txt
Copyright (2012) Lawrence Livermore National Security, LLC.
Produced at the Lawrence Livermore National Laboratory.
Written by Robert E. Rudd (<robert.rudd@llnl.gov>).
Based on the spline MEAM routine written by Alexander Stukowski
(<alex@stukowski.com>).
LLNL-CODE-588032 All rights reserved.

The spline-based MEAM+SW format was first devised and used to develop
potentials for bcc transition metals by Jeremy Nicklas, Michael Fellinger,
and Hyoungki Park at The Ohio State University.

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License (as published by the Free
Software Foundation) version 2, dated June 1991.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the terms and conditions of the
GNU General Public License for more details.

Our Preamble Notice
A. This notice is required to be provided under our contract with the
U.S. Department of Energy (DOE). This work was produced at the
Lawrence Livermore National Laboratory under Contract No.
DE-AC52-07NA27344 with the DOE.

B. Neither the United States Government nor Lawrence Livermore National
Security, LLC nor any of their employees, makes any warranty, express or
implied, or assumes any liability or responsibility for the accuracy,
completeness, or usefulness of any information, apparatus, product, or
process disclosed, or represents that its use would not infringe
privately-owned rights.

C. Also, reference herein to any specific commercial products, process,
or services by trade name, trademark, manufacturer or otherwise does not
necessarily constitute or imply its endorsement, recommendation, or
favoring by the United States Government or Lawrence Livermore National
Security, LLC. The views and opinions of authors expressed herein do not
necessarily state or reflect those of the United States Government or
Lawrence Livermore National Security, LLC, and shall not be used for
advertising or product endorsement purposes.
 ```

```txt
Copyright (c) 2020, Regents of the University of Minnesota.
All rights reserved.
```

## Contributing

Contributors:\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yaser Afshar (UMN),\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Daniel S. Karls (UMN)

Contributing authors, original code in the LAMMPS USER-MEAMC (MEAM/C):\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Greg Wagner (SNL),\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sebastian Hütter (OvGU)

Contributing authors, original code in the LAMMPS USER-MISC (MEAM/SPLINE):\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alexander Stukowski (LLNL),\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Will Tipton (Cornell),\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dallas R. Trinkle (UIUC),\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pinchao Zhang (UIUC),\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Robert Rudd (LLNL)

Contributing authors, original code in the LAMMPS USER-MISC (MEAM/SW/SPLINE):\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Robert Rudd (LLNL)

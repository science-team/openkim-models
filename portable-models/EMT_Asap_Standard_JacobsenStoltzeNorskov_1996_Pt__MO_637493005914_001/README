Effective Medium Theory (EMT) model based on the EMT implementation in
ASAP (https://wiki.fysik.dtu.dk/asap).  This model uses the
EMT_Asap__MD_128315414717 model driver.

Effective Medium Theory is a many-body potential of the same class as Embedded
Atom Method, Finnis-Sinclair etc.  The main term in the energy per atom is the
local density of atoms.

The functional form implemented here is that of Ref. 1.  The principles behind
EMT are described in Refs. 2 and 3 (with 2 being the more detailed and 3 being
the most pedagogical).  Be aware that the functional form and even some of the
principles have changed since refs 2 and 3.  EMT can be considered the last
step of a series of approximations starting with Density Functional Theory,
see Ref 4.

This model implements the "official" parametrization as published in Ref. 1.

This parametrization is appropriate for single-element simulations of platinum
(Pt).  For alloy simulations, please use the alloy parametrization
EMT_Asap_Standard_Jacobsen_Stoltze_Norskov_AlAgAuCuNiPdPt which uses a
slightly larger cutoff to accomodate for all the elements, at the
price of changing the properties of the individual elements
marginally.

These files are based on Asap version 3.11.5.


REFERENCES:

[1] Jacobsen, K. W., Stoltze, P., & Nørskov, J.: "A semi-empirical effective
medium theory for metals and alloys". Surf. Sci. 366, 394–402  (1996).

[2] Jacobsen, K. W., Nørskov, J., & Puska, M.: "Interatomic interactions in
the effective-medium theory". Phys. Rev. B 35, 7423–7442 (1987).

[3] Jacobsen, K. W.: "Bonding in Metallic Systems: An Effective-Medium
Approach".  Comments Cond. Mat. Phys. 14, 129-161 (1988).

[4] Chetty, N., Stokbro, K., Jacobsen, K. W., & Nørskov, J.: "Ab initio
potential for solids". Phys. Rev. B 46, 3798–3809 (1992).


KNOWN ISSUES / BUGS:

* On-the-fly modifications of the parameters is not supported.  It should be
  implemented.



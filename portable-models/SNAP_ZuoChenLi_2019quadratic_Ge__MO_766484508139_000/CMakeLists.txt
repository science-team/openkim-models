#
# Required preamble
#

cmake_minimum_required(VERSION 3.4)

list(APPEND CMAKE_PREFIX_PATH $ENV{KIM_API_CMAKE_PREFIX_DIR})
find_package(KIM-API 2.0 REQUIRED CONFIG)
if(NOT TARGET kim-api)
  enable_testing()
  project("${KIM_API_PROJECT_NAME}" VERSION "${KIM_API_VERSION}"
    LANGUAGES CXX C Fortran)
endif()

# End preamble


add_kim_api_model_library(
  NAME            "SNAP_ZuoChenLi_2019quadratic_Ge__MO_766484508139_000"
  DRIVER_NAME     "SNAP__MD_536750310735_000"
  PARAMETER_FILES "Ge_Zuo_Arxiv2019.qsnapcoeff"
                  "Ge_Zuo_Arxiv2019.qsnapparam"
  )

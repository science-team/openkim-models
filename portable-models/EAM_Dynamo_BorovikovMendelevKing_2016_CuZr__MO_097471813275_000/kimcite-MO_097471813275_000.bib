@Comment
{
\documentclass{article}
\usepackage{url}
\begin{document}
This Model originally published in \cite{MO_097471813275_000a} is archived in OpenKIM~\cite{MO_097471813275_000, MD_120291908751_005, tadmor:elliott:2011, elliott:tadmor:2011}.
\bibliographystyle{vancouver}
\bibliography{kimcite-MO_097471813275_000.bib}
\end{document}
}

@Misc{MO_097471813275_000,
  author       = {Ellad Tadmor},
  title        = {{F}innis-{S}inclair potential ({LAMMPS} cubic hermite tabulation) for the {C}u-{Z}r system developed by {B}orovikov, {M}endelev and {K}ing (2016) v000},
  doi          = {10.25950/e49bf93d},
  howpublished = {OpenKIM, \url{https://doi.org/10.25950/e49bf93d}},
  keywords     = {OpenKIM, Model, MO_097471813275_000},
  publisher    = {OpenKIM},
  year         = 2018,
}

@Misc{MD_120291908751_005,
  author       = {Ryan S. Elliott},
  title        = {{EAM} {M}odel {D}river for tabulated potentials with cubic {H}ermite spline interpolation as used in {LAMMPS} v005},
  doi          = {10.25950/68defa36},
  howpublished = {OpenKIM, \url{https://doi.org/10.25950/e49bf93d}},
  keywords     = {OpenKIM, Model Driver, MD_120291908751_005},
  publisher    = {OpenKIM},
  year         = 2018,
}

@Article{tadmor:elliott:2011,
  author    = {E. B. Tadmor and R. S. Elliott and J. P. Sethna and R. E. Miller and C. A. Becker},
  title     = {The potential of atomistic simulations and the {K}nowledgebase of {I}nteratomic {M}odels},
  journal   = {{JOM}},
  year      = {2011},
  volume    = {63},
  number    = {7},
  pages     = {17},
  doi       = {10.1007/s11837-011-0102-6},
}

@Misc{elliott:tadmor:2011,
  author       = {Ryan S. Elliott and Ellad B. Tadmor},
  title        = {{K}nowledgebase of {I}nteratomic {M}odels ({KIM}) Application Programming Interface ({API})},
  howpublished = {\url{https://openkim.org/kim-api}},
  publisher    = {OpenKIM},
  year         = 2011,
  doi          = {10.25950/ff8f563a},
}

@Article{MO_097471813275_000a,
  author = {Borovikov, Valery and Mendelev, Mikhail I and King, Alexander H},
  doi = {10.1088/0965-0393/24/8/085017},
  journal = {Modelling and Simulation in Materials Science and Engineering},
  number = {8},
  pages = {085017},
  title = {Effects of stable and unstable stacking fault energy on dislocation nucleation in nano-crystalline metals},
  volume = {24},
  year = {2016},
}

{
 "creator" "Jakob Schiøtz"
 "content-origin" "https://gitlab.com/asap/asap"
 "contributor-id" "cd17c370-4feb-4b65-8830-079f1ed1c17f"
 "description" "Effective Medium Theory (EMT) model based on the EMT implementation in ASAP (https://wiki.fysik.dtu.dk/asap).\n\nEffective Medium Theory is a many-body potential of the same class as Embedded Atom Method, Finnis-Sinclair etc.  The main term in the energy per atom is the local density of atoms.\n\nThe functional form implemented here is that of Ref. 1.  The principles behind EMT are described in Refs. 2 and 3 (with 2 being the more detailed and 3 being the most pedagogical).  Be aware that the functional form and even some of the principles have changed since refs 2 and 3.  EMT can be considered the last step of a series of approximations starting with Density Functional Theory; see Ref 4.\n\nThis model implements a special parametrization optimized for CuZr [5] bulk metallic glasses only!  It probably gives reasonable results for other CuZr compounds.\n\nThese files are based on Asap version 3.11.5.\n\n\nREFERENCES:\n\n[1] Jacobsen, K. W., Stoltze, P., & Nørskov, J.: \"A semi-empirical effective medium theory for metals and alloys\". Surf. Sci. 366, 394–402  (1996).\n\n[2] Jacobsen, K. W., Nørskov, J., & Puska, M.: \"Interatomic interactions in the effective-medium theory\". Phys. Rev. B 35, 7423–7442 (1987).\n\n[3] Jacobsen, K. W.: \"Bonding in Metallic Systems: An Effective-Medium Approach\".  Comments Cond. Mat. Phys. 14, 129-161 (1988).\n\n[4] Chetty, N., Stokbro, K., Jacobsen, K. W., & Nørskov, J.: \"Ab initio potential for solids\". Phys. Rev. B 46, 3798–3809 (1992).\n\n[5] Paduraru, A., Kenoufi, A., Bailey, N. P., & Schiøtz, J.:  \"An interatomic potential for studying CuZr bulk metallic glasses\". Adv. Eng. Mater. 9, 505–508 (2007).\n\n\nKNOWN ISSUES / BUGS:\n* On-the-fly modifications of the parameters is not supported, and should be implemented in the future."
 "doi" "10.25950/82d2cfd9"
 "domain" "openkim.org"
 "extended-id" "EMT_Asap_MetalGlass_PaduraruKenoufiBailey_2007_CuZr__MO_987541074959_001"
 "kim-api-version" "2.0.2"
 "maintainer-id" "cd17c370-4feb-4b65-8830-079f1ed1c17f"
 "model-driver" "EMT_Asap__MD_128315414717_004"
 "potential-type" "eam"
 "publication-year" "2019"
 "source-citations" [
  {
   "author" "K. W. Jacobsen and P. Stoltze and J. K. N{\\o}rskov"
   "doi" "10.1016/0039-6028(96)00816-3"
   "issn" "0039-6028"
   "journal" "Surface Science"
   "number" "2"
   "pages" "394--402"
   "recordkey" "MO_987541074959_001a"
   "recordtype" "article"
   "title" "A semi-empirical effective medium theory for metals and alloys"
   "volume" "366"
   "year" "1996"
  }
  {
   "author" "P{\\v a}duraru A. and Kenoufi A. and Bailey N. P. and Schi{\\o}tz J."
   "doi" "10.1002/adem.200700047"
   "journal" "Advanced Engineering Materials"
   "number" "6"
   "pages" "505--508"
   "recordkey" "MO_987541074959_001b"
   "recordprimary" "recordprimary"
   "recordtype" "article"
   "title" "An Interatomic Potential for Studying {CuZr} Bulk Metallic Glasses"
   "volume" "9"
   "year" "2007"
  }
 ]
 "species" [
  "Cu"
  "Zr"
 ]
 "title" "EMT potential for Cu-Zr metallic glasses developed by Paduraru et al. (2007) v001"
}

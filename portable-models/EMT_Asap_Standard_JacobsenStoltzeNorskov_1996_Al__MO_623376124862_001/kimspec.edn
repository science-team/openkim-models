{
 "creator" "Jakob Schiøtz"
 "contributor-id" "cd17c370-4feb-4b65-8830-079f1ed1c17f"
 "description" "Effective Medium Theory (EMT) model based on the EMT implementation in ASAP (https://wiki.fysik.dtu.dk/asap).\n\nEffective Medium Theory is a many-body potential of the same class as Embedded Atom Method, Finnis-Sinclair etc.  The main term in the energy per atom is the local density of atoms.\n\nThe functional form implemented here is that of Ref. 1.  The principles behind EMT are described in Refs. 2 and 3 (with 2 being the more detailed and 3 being the most pedagogical).  Be aware that the functional form and even some of the principles have changed since refs 2 and 3.  EMT can be considered the last step of a series of approximations starting with Density Functional Theory; see Ref 4.\n\nThis model implements the \"official\" parametrization as published in Ref. 1.\n\nThis parametrization is appropriate for single-element simulations of aluminium (Al).  For alloy simulations, please use the alloy parametrization EMT_Asap_Standard_JacobsenStoltzeNorskov_1996_AlAgAuCuNiPdPt which uses a slightly larger cutoff to accomodate for all the elements, at the price of changing the properties of the individual elements marginally.\n\n\nThese files are based on Asap version 3.11.5.\n\n\nREFERENCES:\n\n[1] Jacobsen, K. W., Stoltze, P., & Nørskov, J.: \"A semi-empirical effective medium theory for metals and alloys\". Surf. Sci. 366, 394–402  (1996).\n\n[2] Jacobsen, K. W., Nørskov, J., & Puska, M.: \"Interatomic interactions in the effective-medium theory\". Phys. Rev. B 35, 7423–7442 (1987).\n\n[3] Jacobsen, K. W.: \"Bonding in Metallic Systems: An Effective-Medium Approach\".  Comments Cond. Mat. Phys. 14, 129-161 (1988).\n\n[4] Chetty, N., Stokbro, K., Jacobsen, K. W., & Nørskov, J.: \"Ab initio potential for solids\". Phys. Rev. B 46, 3798–3809 (1992).\n\n\nKNOWN ISSUES / BUGS:\n* On-the-fly modifications of the parameters is not supported, and should be implemented in the future."
 "doi" "10.25950/bdbaee6a"
 "domain" "openkim.org"
 "extended-id" "EMT_Asap_Standard_JacobsenStoltzeNorskov_1996_Al__MO_623376124862_001"
 "kim-api-version" "2.0.2"
 "maintainer-id" "cd17c370-4feb-4b65-8830-079f1ed1c17f"
 "model-driver" "EMT_Asap__MD_128315414717_004"
 "potential-type" "eam"
 "publication-year" "2019"
 "source-citations" [
  {
   "author" "K. W. Jacobsen and P. Stoltze and J. K. N{\\o}rskov"
   "doi" "10.1016/0039-6028(96)00816-3"
   "issn" "0039-6028"
   "journal" "Surface Science"
   "number" "2"
   "pages" "394--402"
   "recordkey" "MO_623376124862_001a"
   "recordprimary" "recordprimary"
   "recordtype" "article"
   "title" "A semi-empirical effective medium theory for metals and alloys"
   "volume" "366"
   "year" "1996"
  }
 ]
 "species" [
  "Al"
 ]
 "title" "EMT potential for Al developed by Jacobsen, Stoltze, and Norskov (1996) v001"
}

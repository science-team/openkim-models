#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the Common Development
# and Distribution License Version 1.0 (the "License").
#
# You can obtain a copy of the license at
# http://www.opensource.org/licenses/CDDL-1.0.  See the License for the
# specific language governing permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each file and
# include the License file in a prominent location with the name LICENSE.CDDL.
# If applicable, add the following below this CDDL HEADER, with the fields
# enclosed by brackets "[]" replaced with your own identifying information:
#
# Portions Copyright (c) [yyyy] [name of copyright owner]. All rights reserved.
#
# CDDL HEADER END
#

#
# Copyright (c) 2012,   Institute for Theoretical and Applied Physics
#      			University of Stuttgart, D-70550 Stuttgart, Germany.
# 			All rights reserved.

#
# Contributors:
#    Daniel Schopf
#

#
# Required preamble
#

cmake_minimum_required(VERSION 3.4)

list(APPEND CMAKE_PREFIX_PATH $ENV{KIM_API_CMAKE_PREFIX_DIR})
find_package(KIM-API 2.0 REQUIRED CONFIG)
if(NOT TARGET kim-api)
  enable_testing()
  project("${KIM_API_PROJECT_NAME}" VERSION "${KIM_API_VERSION}"
    LANGUAGES CXX C Fortran)
endif()

# End preamble


add_kim_api_model_library(
  NAME            "EAM_IMD_BrommerBoissieuEuchner_2009_MgZn__MO_710767216198_003"
  DRIVER_NAME     "EAM_IMD__MD_113599595631_003"
  PARAMETER_FILES "species.params" "mgzn.222_gga.21719_new_phi.imd.pt" "mgzn.222_gga.21719_new_rho.imd.pt" "mgzn.222_gga.21719_new_F.imd.pt"
  )

{
 "creator" "Daniel S. Karls"
 "contributor-id" "4d62befd-21c4-42b8-a472-86132e6591f3"
 "description" "We develop an empirical potential for silicon which represents a considerable improvement over existing models in describing local bonding for bulk defects and disordered phases. The model consists of two- and three-body interactions with theoretically motivated functional forms that capture chemical and physical trends as explained in a companion paper. The numerical parameters in the functional form are obtained by fitting to a set of ab initio results from quantum-mechanical calculations based on density-functional theory in the local-density approximation, which include various bulk phases and defect structures. We test the potential by applying it to the relaxation of point defects, core properties of partial dislocations and the structure of disordered phases, none of which are included in the fitting procedure. For dislocations, our model makes predictions in excellent agreement with ab initio and tight-binding calculations. It is the only potential known to describe both the 30°- and 90°-partial dislocations in the glide set {111}. The structural and thermodynamic properties of the liquid and amorphous phases are also in good agreement with experimental and ab initio results. Our potential is capable of simulating a quench directly from the liquid to the amorphous phase, and the resulting amorphous structure is more realistic than with existing empirical preparation methods. These advances in transferability come with no extra computational cost, since force evaluation with our model is faster than with the popular potential of Stillinger-Weber, thus allowing reliable atomistic simulations of very large atomic systems."
 "doi" "10.25950/545ca247"
 "domain" "openkim.org"
 "extended-id" "EDIP_JustoBazantKaxiras_1998_Si__MO_958932894036_002"
 "kim-api-version" "2.0"
 "maintainer-id" "4d62befd-21c4-42b8-a472-86132e6591f3"
 "model-driver" "EDIP__MD_506186535567_002"
 "potential-type" "edip"
 "publication-year" "2018"
 "source-citations" [
  {
   "author" "Justo, Jo\\~{a}o F. and Bazant, Martin Z. and Kaxiras, Efthimios and Bulatov, V. V. and Yip, Sidney"
   "doi" "10.1103/PhysRevB.58.2539"
   "issue" "5"
   "journal" "Physical Review B"
   "month" "Aug"
   "pages" "2539--2550"
   "publisher" "American Physical Society"
   "recordkey" "MO_958932894036_002a"
   "recordprimary" "recordprimary"
   "recordtype" "article"
   "title" "Interatomic potential for silicon defects and disordered phases"
   "volume" "58"
   "year" "1998"
  }
 ]
 "species" [
  "Si"
 ]
 "title" "EDIP model for Si developed by Justo et al. (1998) v002"
}

#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the Common Development
# and Distribution License Version 1.0 (the "License").
#
# You can obtain a copy of the license at
# http://www.opensource.org/licenses/CDDL-1.0.  See the License for the
# specific language governing permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each file and
# include the License file in a prominent location with the name LICENSE.CDDL.
# If applicable, add the following below this CDDL HEADER, with the fields
# enclosed by brackets "[]" replaced with your own identifying information:
#
# Portions Copyright (c) [yyyy] [name of copyright owner]. All rights reserved.
#
# CDDL HEADER END
#
#
# Copyright (c) 2013--2019, Regents of the University of Minnesota.
# All rights reserved.
#
# Contributors:
#    Ellad B. Tadmor
#


This directory contains a Model based on the Tunable Intrinsic Ductility
Potential (TIDP) KIM Model Driver. This is an interatomic pair potential
with tunable intrinsic ductility developed by

V. P. Rajan, D. H. Warner and W. A. Curtin,
Model. Simul. Mater. Sci. Eng., 24:025005, 2016.
https://doi.org/10.1088/0965-0393/24/2/025005

This Model implements the D (intermediate ductile/brittle) parameterization.

Note that this is a "toy model" in the sense that it does
not represent any real material. It is therefore associated
with species user01.

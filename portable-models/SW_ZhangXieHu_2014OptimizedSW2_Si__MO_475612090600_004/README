#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the Common Development
# and Distribution License Version 1.0 (the "License").
#
# You can obtain a copy of the license at
# http://www.opensource.org/licenses/CDDL-1.0.  See the License for the
# specific language governing permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each file and
# include the License file in a prominent location with the name LICENSE.CDDL.
# If applicable, add the following below this CDDL HEADER, with the fields
# enclosed by brackets "[]" replaced with your own identifying information:
#
# Portions Copyright (c) [yyyy] [name of copyright owner]. All rights reserved.
#
# CDDL HEADER END
#

#
# Copyright (c) 2012, Regents of the University of Minnesota.  All rights reserved.
#
# Contributors:
#    Ryan S. Elliott
#    Ellad B. Tadmor
#    Valeriu Smirichinski
#    Amit Singh
#    Mingjian Wen


This directory contains a parameterization of the Stillinger-Weber
potential [1] optimized for Silicene (single-layer Si sheets) [2]
based on the KIM Model Driver Three_Body_Stillinger_Weber.  According
to the authors, the potential "accurately reproduces the low buckling
structure of silicene and the full phonon dispersion curves obtained
from ab initio calculations" [2].  The authors provide two sets of
parameters in the paper. This parameterization corresponds to the
parameter set referred to as "Optimized SW2" in [2].

References:

1. F. H. Stillinger and T. A. Weber, "Computer simulation of local order in condensed phases of silicon", Phys. Rev. B, vol. 31, 5262-5271, 1985

2. X. Zhang, H. Xie, M. Hu, H. Bao, S. Yue, G. Qin and G. Su, "Thermal conductivity of silicene calculated using an optimized Stillinger-Weber potential", Phys. Rev. B, vol 89, 054310, 2014.



###############################################################################
The functional form of the SW potential is rewritten as:

  E = sum_{i,j>i} phi_2(rij) + sum_{i, j!=i, k>j} phi_3(rij, rik, theta)

  phi_2(rij)           = Aij*(Bij(rij/sigma_ij)^(-p) - (rij/sigma_ij)^(-q))
                         * exp(sigma_ij/(rij - rcutij))

  phi_3(rij,rik,theta) = lambda_ijk(cos[theta] - cos[theta0])^2
                         *  exp(gamma_ij/(rij - rcutij) + gamma_ik/(rik - rcutik))

Compared to the standard SW potential, the following redefinitions have been made:
  A      := A*epsilon
  lambda := lambda*epsilon
  gamma  := gamma*sigma
  rcut   := a*sigma

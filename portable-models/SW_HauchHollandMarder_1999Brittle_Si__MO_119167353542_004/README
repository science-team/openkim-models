#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the Common Development
# and Distribution License Version 1.0 (the "License").
#
# You can obtain a copy of the license at
# http://www.opensource.org/licenses/CDDL-1.0.  See the License for the
# specific language governing permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each file and
# include the License file in a prominent location with the name LICENSE.CDDL.
# If applicable, add the following below this CDDL HEADER, with the fields
# enclosed by brackets "[]" replaced with your own identifying information:
#
# Portions Copyright (c) [yyyy] [name of copyright owner]. All rights reserved.
#
# CDDL HEADER END
#

#
# Copyright (c) 2012, Regents of the University of Minnesota.  All rights reserved.
#
# Contributors:
#    Ryan S. Elliott
#    Ellad B. Tadmor
#    Valeriu Smirichinski
#    Amit Singh
#    Mingjian Wen


This directory contains the rescaled Stillinger-Weber potential due to
Hauch et al. (1999).  This Model implements the Stillinger-Weber (SW)
potential for Si with a rescaled value of lambda parameter used in the
SW potential. The original lambda value was 21.0 [Ref 1], which was
doubled by Hauch et al [Ref 2].

References:

1. F. H. Stillinger and T. A. Weber, "Computer simulation of local order in condensed phases of silicon", Phys. Rev. B, vol. 31, 5262-5271, 1985

2. Jens A. Hauch, Dominic Holland, M. P. Marder, and Harry L. Swinney, "Dynamic Fracture in Single Crystal Silicon", Phys. Rev. Lett., vol 82, 3823--3826, 1999

3. Ellad B. Tadmor and Ronald E. Miller, Modeling Materials: Continuum, Atomistic and Multiscale Techniques, Cambridge University Press, 2011



###############################################################################
The functional form of the SW potential is rewritten as:

  E = sum_{i,j>i} phi_2(rij) + sum_{i, j!=i, k>j} phi_3(rij, rik, theta)

  phi_2(rij)           = Aij*(Bij(rij/sigma_ij)^(-p) - (rij/sigma_ij)^(-q))
                         * exp(sigma_ij/(rij - rcutij))

  phi_3(rij,rik,theta) = lambda_ijk(cos[theta] - cos[theta0])^2
                         *  exp(gamma_ij/(rij - rcutij) + gamma_ik/(rik - rcutik))

Compared to the standard SW potential, the following redefinitions have been made:
  A      := A*epsilon
  lambda := lambda*epsilon
  gamma  := gamma*sigma
  rcut   := a*sigma

#
# Author: Tobias Brink
#

cmake_minimum_required(VERSION 3.10)

list(APPEND CMAKE_PREFIX_PATH $ENV{KIM_API_CMAKE_PREFIX_DIR})
find_package(KIM-API-ITEMS 2.2 REQUIRED CONFIG)

kim_api_items_setup_before_project(ITEM_TYPE "portableModel")
project(Tersoff_LAMMPS_ErhartJuslinGoy_2006_ZnO__MO_616776018688_003)
kim_api_items_setup_after_project(ITEM_TYPE "portableModel")

add_kim_api_model_library(
  NAME            ${PROJECT_NAME}
  DRIVER_NAME     "Tersoff_LAMMPS__MD_077075034781_004"
  PARAMETER_FILES "Tersoff_ErhartJuslinGoy_2006_ZnO.settings"
                  "Tersoff_ErhartJuslinGoy_2006_ZnO.params"
  )

cmake_minimum_required(VERSION 3.4)

list(APPEND CMAKE_PREFIX_PATH $ENV{KIM_API_CMAKE_PREFIX_DIR})
find_package(KIM-API 2.0 REQUIRED CONFIG)
if(NOT TARGET kim-api)
  enable_testing()
  project("${KIM_API_PROJECT_NAME}" VERSION "${KIM_API_VERSION}"
    LANGUAGES CXX C Fortran)
endif()

# End preamble


add_kim_api_model_library(
  NAME            "TIDP_RajanWarnerCurtin_2016A_User01__MO_514760222899_001"
  DRIVER_NAME     "TIDP__MD_167784395616_000"
  PARAMETER_FILES "TIDP_RajanWarnerCurtin_2016A_dp.params"
  )
